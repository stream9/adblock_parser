#include "src/filter_rule.hpp"

#include <adblock_parser/char_type.hpp>
#include <adblock_parser/event_handler.hpp>

#include <string>
#include <string_view>

#include <boost/test/unit_test.hpp>

namespace adblock_parser::testing {

using namespace std::literals;

BOOST_AUTO_TEST_SUITE(filter_rule_)

    struct handler : public event_handler
    {
        void
        error(iterator begin, iterator /*end*/, std::string_view msg) override
        {
            this->error_pos = begin;
            this->error_msg = msg;
        }

        iterator error_pos;
        std::string error_msg;
    };

    BOOST_AUTO_TEST_SUITE(basic_filter_rule_)

        BOOST_AUTO_TEST_CASE(ok1_)
        {
            auto const s = "pattern"sv;
            handler h;

            BOOST_TEST(basic_filter_rule(s.begin(), s.end(), s.end(), h));
        }

        BOOST_AUTO_TEST_CASE(ok2_)
        {
            auto const s = "$script"sv;
            handler h;

            BOOST_TEST(basic_filter_rule(s.begin(), s.end(), s.end(), h));
        }

        BOOST_AUTO_TEST_CASE(ok3_)
        {
            auto const s = "pattern$script"sv;
            handler h;

            auto const ok = basic_filter_rule(s.begin(), s.begin() + 7, s.end(), h);
            BOOST_TEST(ok);
        }

        BOOST_AUTO_TEST_CASE(error1_)
        {
            auto const s = "pattern$"sv;
            handler h;

            auto const ok = basic_filter_rule(s.begin(), s.begin() + 7, s.end(), h);

            BOOST_REQUIRE(!ok);
            BOOST_TEST(h.error_msg == "empty option");
            BOOST_TEST((h.error_pos - s.begin()) == 8);
        }

    BOOST_AUTO_TEST_SUITE_END() // basic_filter_rule_

    BOOST_AUTO_TEST_SUITE(exception_filter_rule_)

        BOOST_AUTO_TEST_CASE(ok1_)
        {
            auto const s = "@@pattern"sv;
            handler h;

            auto const ok =
                exception_filter_rule(s.begin(), s.end(), s.end(), h);

            BOOST_REQUIRE(ok);
        }

        BOOST_AUTO_TEST_CASE(ok2_)
        {
            auto const s = "@@$script,~image"sv;
            handler h;

            auto const ok =
                exception_filter_rule(s.begin(), s.begin() + 2, s.end(), h);

            BOOST_REQUIRE(ok);
        }

        BOOST_AUTO_TEST_CASE(ok3_)
        {
            auto const s = "@@pattern$script,~image"sv;
            handler h;

            auto const ok =
                exception_filter_rule(s.begin(), s.begin() + 9, s.end(), h);

            BOOST_REQUIRE(ok);
        }

        BOOST_AUTO_TEST_CASE(error1_)
        {
            auto const s = "@@"sv;
            handler h;

            auto const ok =
                exception_filter_rule(s.begin(), s.end(), s.end(), h);

            BOOST_REQUIRE(!ok);
            BOOST_TEST(h.error_msg == "empty exception filter rule");
            BOOST_TEST((h.error_pos - s.begin()) == 2);
        }

        BOOST_AUTO_TEST_CASE(error2_)
        {
            auto const s = "@@||"sv;
            handler h;

            auto const ok =
                exception_filter_rule(s.begin(), s.end(), s.end(), h);

            BOOST_REQUIRE(!ok);
            BOOST_TEST(h.error_msg == "constrained empty pattern");
            BOOST_TEST((h.error_pos - s.begin()) == 2);
        }

    BOOST_AUTO_TEST_SUITE_END() // exception_filter_rule_

BOOST_AUTO_TEST_SUITE_END() // filter_rule_

} // namespace adblock_parser::testing
