#include "src/filter_option.hpp"

#include <adblock_parser/char_type.hpp>
#include <adblock_parser/event_handler.hpp>

#include "src/error.hpp"
#include "src/utility.hpp"

#include <boost/test/unit_test.hpp>

namespace adblock_parser::testing {

using namespace std::literals;

BOOST_AUTO_TEST_SUITE(filter_option_)

    struct handler : public event_handler
    {
        void begin_filter_options(iterator, iterator) override
        {
            this->options = {};
        }

        void end_filter_options(iterator, iterator) override
        {
            this->got_options = true;
        }

        void script_option(iterator, iterator, bool inverse_) override
        {
            this->name = "script";
            this->inverse = inverse_;
            this->options.push_back(this->name);
        }

        void image_option(iterator, iterator, bool inverse_) override
        {
            this->name = "image";
            this->inverse = inverse_;
            this->options.push_back(this->name);
        }

        void stylesheet_option(iterator, iterator, bool inverse_) override
        {
            this->name = "stylesheet";
            this->inverse = inverse_;
            this->options.push_back(this->name);
        }

        void object_option(iterator, iterator, bool inverse_) override
        {
            this->name = "object";
            this->inverse = inverse_;
            this->options.push_back(this->name);
        }

        void xmlhttprequest_option(iterator, iterator, bool inverse_) override
        {
            this->name = "xmlhttprequest";
            this->inverse = inverse_;
            this->options.push_back(this->name);
        }

        void object_subrequest_option(iterator, iterator, bool inverse_) override
        {
            this->name = "object_subrequest";
            this->inverse = inverse_;
            this->options.push_back(this->name);
        }

        void subdocument_option(iterator, iterator, bool inverse_) override
        {
            this->name = "subdocument";
            this->inverse = inverse_;
            this->options.push_back(this->name);
        }

        void ping_option(iterator, iterator, bool inverse_) override
        {
            this->name = "ping";
            this->inverse = inverse_;
            this->options.push_back(this->name);
        }

        void websocket_option(iterator, iterator, bool inverse_) override
        {
            this->name = "websocket";
            this->inverse = inverse_;
            this->options.push_back(this->name);
        }

        void webrtc_option(iterator, iterator, bool inverse_) override
        {
            this->name = "webrtc";
            this->inverse = inverse_;
            this->options.push_back(this->name);
        }

        void popup_option(iterator, iterator) override
        {
            this->name = "popup";
            this->options.push_back(this->name);
        }

        void media_option(iterator, iterator, bool inverse_) override
        {
            this->name = "media";
            this->inverse = inverse_;
            this->options.push_back(this->name);
        }

        void font_option(iterator, iterator, bool inverse_) override
        {
            this->name = "font";
            this->inverse = inverse_;
            this->options.push_back(this->name);
        }

        void other_option(iterator, iterator, bool inverse_) override
        {
            this->name = "other";
            this->inverse = inverse_;
            this->options.push_back(this->name);
        }

        void third_party_option(iterator, iterator, bool inverse_) override
        {
            this->name = "third_party";
            this->inverse = inverse_;
            this->options.push_back(this->name);
        }

        void match_case_option(iterator, iterator) override
        {
            this->name = "match_case";
            this->options.push_back(this->name);
        }

        void collapse_option(iterator, iterator, bool inverse_) override
        {
            this->name = "collapse";
            this->inverse = inverse_;
            this->options.push_back(this->name);
        }

        void do_not_track_option(iterator, iterator) override
        {
            this->name = "do_not_track";
            this->options.push_back(this->name);
        }

        void begin_domain_option(iterator /*begin*/, iterator /*end*/) override
        {
            this->domains = {};
        }

        void filter_domain(iterator begin, iterator end) override
        {
            this->domains.push_back(make_string_view(begin, end));
        }

        void end_domain_option(iterator /*begin*/, iterator /*end*/) override
        {
            this->name = "domain";
            this->options.push_back(this->name);
        }

        void site_key_option(iterator, iterator, iterator begin, iterator end) override
        {
            this->name = "site_key";
            this->value = make_string_view(begin, end);
            this->options.push_back(this->name);
        }

        void csp_option(iterator, iterator, iterator begin, iterator end) override
        {
            this->name = "csp";
            this->value = make_string_view(begin, end);
            this->options.push_back(this->name);
        }

        void rewrite_option(iterator, iterator, iterator begin, iterator end) override
        {
            this->name = "rewrite";
            this->value = make_string_view(begin, end);
            this->options.push_back(this->name);
        }

        void document_option(iterator, iterator, bool inverse_) override
        {
            this->name = "document";
            this->inverse = inverse_;
            this->options.push_back(this->name);
        }

        void elem_hide_option(iterator, iterator, bool inverse_) override
        {
            this->name = "elemhide";
            this->inverse = inverse_;
            this->options.push_back(this->name);
        }

        void generic_hide_option(iterator, iterator) override
        {
            this->name = "generic_hide";
            this->options.push_back(this->name);
        }

        void generic_block_option(iterator, iterator) override
        {
            this->name = "generic_block";
            this->options.push_back(this->name);
        }

        std::string_view name;
        bool inverse = false;
        std::vector<std::string_view> domains;
        std::string_view value;
        std::vector<std::string_view> options;
        bool got_options = false;
    };

    BOOST_AUTO_TEST_SUITE(script_option_)

        BOOST_AUTO_TEST_CASE(ok1_)
        {
            auto const s = "script"sv;
            handler h;

            BOOST_REQUIRE(black_list_option(s.begin(), s.end(), h));
            BOOST_TEST(h.name == "script");
            BOOST_TEST(!h.inverse);
        }

        BOOST_AUTO_TEST_CASE(ok2_)
        {
            auto const s = "  ~script\t"sv;
            handler h;

            BOOST_REQUIRE(black_list_option(s.begin(), s.end(), h));
            BOOST_TEST(h.name == "script");
            BOOST_TEST(h.inverse);
        }

        BOOST_AUTO_TEST_CASE(ok3_)
        {
            auto const s = "SCRIPT"sv;
            handler h;

            BOOST_REQUIRE(black_list_option(s.begin(), s.end(), h));
            BOOST_TEST(h.name == "script");
            BOOST_TEST(!h.inverse);
        }

        BOOST_AUTO_TEST_CASE(error1_)
        {
            auto const s = "script=1"sv;
            handler h;

            BOOST_CHECK_THROW(
                black_list_option(s.begin(), s.end(), h),
                parse_error
            );
        }

    BOOST_AUTO_TEST_SUITE_END() // script_option_

    BOOST_AUTO_TEST_SUITE(image_option_)

        BOOST_AUTO_TEST_CASE(ok1_)
        {
            auto const s = "image"sv;
            handler h;

            auto const ok = black_list_option(s.begin(), s.end(), h);

            BOOST_REQUIRE(ok);
            BOOST_TEST(h.name == "image");
            BOOST_TEST(!h.inverse);
        }

        BOOST_AUTO_TEST_CASE(ok2_)
        {
            auto const s = "~image"sv;
            handler h;

            auto const ok = black_list_option(s.begin(), s.end(), h);

            BOOST_REQUIRE(ok);
            BOOST_TEST(h.name == "image");
            BOOST_TEST(h.inverse);
        }

        BOOST_AUTO_TEST_CASE(error1_)
        {
            auto const s = "image=1"sv;
            handler h;

            BOOST_CHECK_THROW(
                black_list_option(s.begin(), s.end(), h),
                parse_error
            );
        }

    BOOST_AUTO_TEST_SUITE_END() // image_option_

    BOOST_AUTO_TEST_SUITE(stylesheet_option_)

        BOOST_AUTO_TEST_CASE(ok1_)
        {
            auto const s = "stylesheet"sv;
            handler h;

            auto const ok = black_list_option(s.begin(), s.end(), h);

            BOOST_REQUIRE(ok);
            BOOST_TEST(h.name == "stylesheet");
            BOOST_TEST(!h.inverse);
        }

        BOOST_AUTO_TEST_CASE(ok2_)
        {
            auto const s = "~stylesheet"sv;
            handler h;

            auto const ok = black_list_option(s.begin(), s.end(), h);

            BOOST_REQUIRE(ok);
            BOOST_TEST(h.name == "stylesheet");
            BOOST_TEST(h.inverse);
        }

        BOOST_AUTO_TEST_CASE(error1_)
        {
            auto const s = "stylesheet=1"sv;
            handler h;

            BOOST_CHECK_THROW(
                black_list_option(s.begin(), s.end(), h),
                parse_error
            );
        }

    BOOST_AUTO_TEST_SUITE_END() // stylesheet_option_

    BOOST_AUTO_TEST_SUITE(object_option_)

        BOOST_AUTO_TEST_CASE(ok1_)
        {
            auto const s = "object"sv;
            handler h;

            auto const ok = black_list_option(s.begin(), s.end(), h);

            BOOST_REQUIRE(ok);
            BOOST_TEST(h.name == "object");
            BOOST_TEST(!h.inverse);
        }

        BOOST_AUTO_TEST_CASE(ok2_)
        {
            auto const s = "~object"sv;
            handler h;

            auto const ok = black_list_option(s.begin(), s.end(), h);

            BOOST_REQUIRE(ok);
            BOOST_TEST(h.name == "object");
            BOOST_TEST(h.inverse);
        }

        BOOST_AUTO_TEST_CASE(error1_)
        {
            auto const s = "object=1"sv;
            handler h;

            BOOST_CHECK_THROW(
                black_list_option(s.begin(), s.end(), h),
                parse_error
            );
        }

    BOOST_AUTO_TEST_SUITE_END() // object_option_

    BOOST_AUTO_TEST_SUITE(xmlhttprequest_option_)

        BOOST_AUTO_TEST_CASE(ok1_)
        {
            auto const s = "xmlhttprequest"sv;
            handler h;

            auto const ok = black_list_option(s.begin(), s.end(), h);

            BOOST_REQUIRE(ok);
            BOOST_TEST(h.name == "xmlhttprequest");
            BOOST_TEST(!h.inverse);
        }

        BOOST_AUTO_TEST_CASE(ok2_)
        {
            auto const s = "~xmlhttprequest"sv;
            handler h;

            auto const ok = black_list_option(s.begin(), s.end(), h);

            BOOST_REQUIRE(ok);
            BOOST_TEST(h.name == "xmlhttprequest");
            BOOST_TEST(h.inverse);
        }

        BOOST_AUTO_TEST_CASE(error1_)
        {
            auto const s = "xmlhttprequest=1"sv;
            handler h;

            BOOST_CHECK_THROW(
                black_list_option(s.begin(), s.end(), h),
                parse_error
            );
        }

    BOOST_AUTO_TEST_SUITE_END() // xmlhttprequest_option_

    BOOST_AUTO_TEST_SUITE(object_subrequest_option_)

        BOOST_AUTO_TEST_CASE(ok1_)
        {
            auto const s = "object-subrequest"sv;
            handler h;

            auto const ok = black_list_option(s.begin(), s.end(), h);

            BOOST_REQUIRE(ok);
            BOOST_TEST(h.name == "object_subrequest");
            BOOST_TEST(!h.inverse);
        }

        BOOST_AUTO_TEST_CASE(ok2_)
        {
            auto const s = "~object-subrequest"sv;
            handler h;

            auto const ok = black_list_option(s.begin(), s.end(), h);

            BOOST_REQUIRE(ok);
            BOOST_TEST(h.name == "object_subrequest");
            BOOST_TEST(h.inverse);
        }

        BOOST_AUTO_TEST_CASE(error1_)
        {
            auto const s = "object-subrequest=1"sv;
            handler h;

            BOOST_CHECK_THROW(
                black_list_option(s.begin(), s.end(), h),
                parse_error
            );
        }

    BOOST_AUTO_TEST_SUITE_END() // object_subrequest_option_

    BOOST_AUTO_TEST_SUITE(subdocument_option_)

        BOOST_AUTO_TEST_CASE(ok1_)
        {
            auto const s = "subdocument"sv;
            handler h;

            auto const ok = black_list_option(s.begin(), s.end(), h);

            BOOST_REQUIRE(ok);
            BOOST_TEST(h.name == "subdocument");
            BOOST_TEST(!h.inverse);
        }

        BOOST_AUTO_TEST_CASE(ok2_)
        {
            auto const s = "~subdocument"sv;
            handler h;

            auto const ok = black_list_option(s.begin(), s.end(), h);

            BOOST_REQUIRE(ok);
            BOOST_TEST(h.name == "subdocument");
            BOOST_TEST(h.inverse);
        }

        BOOST_AUTO_TEST_CASE(error1_)
        {
            auto const s = "subdocument=1"sv;
            handler h;

            BOOST_CHECK_THROW(
                black_list_option(s.begin(), s.end(), h),
                parse_error
            );
        }

    BOOST_AUTO_TEST_SUITE_END() // subdocument_option_

    BOOST_AUTO_TEST_SUITE(ping_option_)

        BOOST_AUTO_TEST_CASE(ok1_)
        {
            auto const s = "ping"sv;
            handler h;

            auto const ok = black_list_option(s.begin(), s.end(), h);

            BOOST_REQUIRE(ok);
            BOOST_TEST(h.name == "ping");
            BOOST_TEST(!h.inverse);
        }

        BOOST_AUTO_TEST_CASE(ok2_)
        {
            auto const s = "~ping"sv;
            handler h;

            auto const ok = black_list_option(s.begin(), s.end(), h);

            BOOST_REQUIRE(ok);
            BOOST_TEST(h.name == "ping");
            BOOST_TEST(h.inverse);
        }

        BOOST_AUTO_TEST_CASE(error1_)
        {
            auto const s = "ping=1"sv;
            handler h;

            BOOST_CHECK_THROW(
                black_list_option(s.begin(), s.end(), h),
                parse_error
            );
        }

    BOOST_AUTO_TEST_SUITE_END() // ping_option_

    BOOST_AUTO_TEST_SUITE(websocket_option_)

        BOOST_AUTO_TEST_CASE(ok1_)
        {
            auto const s = "websocket"sv;
            handler h;

            auto const ok = black_list_option(s.begin(), s.end(), h);

            BOOST_REQUIRE(ok);
            BOOST_TEST(h.name == "websocket");
            BOOST_TEST(!h.inverse);
        }

        BOOST_AUTO_TEST_CASE(ok2_)
        {
            auto const s = "~websocket"sv;
            handler h;

            auto const ok = black_list_option(s.begin(), s.end(), h);

            BOOST_REQUIRE(ok);
            BOOST_TEST(h.name == "websocket");
            BOOST_TEST(h.inverse);
        }

        BOOST_AUTO_TEST_CASE(error1_)
        {
            auto const s = "websocket=1"sv;
            handler h;

            BOOST_CHECK_THROW(
                black_list_option(s.begin(), s.end(), h),
                parse_error
            );
        }

    BOOST_AUTO_TEST_SUITE_END() // websocket_option_

    BOOST_AUTO_TEST_SUITE(webrtc_option_)

        BOOST_AUTO_TEST_CASE(ok1_)
        {
            auto const s = "webrtc"sv;
            handler h;

            auto const ok = black_list_option(s.begin(), s.end(), h);

            BOOST_REQUIRE(ok);
            BOOST_TEST(h.name == "webrtc");
            BOOST_TEST(!h.inverse);
        }

        BOOST_AUTO_TEST_CASE(ok2_)
        {
            auto const s = "~webrtc"sv;
            handler h;

            auto const ok = black_list_option(s.begin(), s.end(), h);

            BOOST_REQUIRE(ok);
            BOOST_TEST(h.name == "webrtc");
            BOOST_TEST(h.inverse);
        }

        BOOST_AUTO_TEST_CASE(error1_)
        {
            auto const s = "webrtc=1"sv;
            handler h;

            BOOST_CHECK_THROW(
                black_list_option(s.begin(), s.end(), h),
                parse_error
            );
        }

    BOOST_AUTO_TEST_SUITE_END() // webrtc_option_

    BOOST_AUTO_TEST_SUITE(popup_option_)

        BOOST_AUTO_TEST_CASE(ok1_)
        {
            auto const s = "popup"sv;
            handler h;

            auto const ok = black_list_option(s.begin(), s.end(), h);

            BOOST_REQUIRE(ok);
            BOOST_TEST(h.name == "popup");
        }

        BOOST_AUTO_TEST_CASE(error1_)
        {
            auto const s = "popup=1"sv;
            handler h;

            BOOST_CHECK_THROW(
                black_list_option(s.begin(), s.end(), h),
                parse_error
            );
        }

        BOOST_AUTO_TEST_CASE(error2_)
        {
            auto const s = "~popup"sv;
            handler h;

            BOOST_CHECK_THROW(
                black_list_option(s.begin(), s.end(), h),
                parse_error
            );
        }

    BOOST_AUTO_TEST_SUITE_END() // popup_option_

    BOOST_AUTO_TEST_SUITE(media_option_)

        BOOST_AUTO_TEST_CASE(ok1_)
        {
            auto const s = "media"sv;
            handler h;

            auto const ok = black_list_option(s.begin(), s.end(), h);

            BOOST_REQUIRE(ok);
            BOOST_TEST(h.name == "media");
            BOOST_TEST(!h.inverse);
        }

        BOOST_AUTO_TEST_CASE(ok2_)
        {
            auto const s = "~media"sv;
            handler h;

            auto const ok = black_list_option(s.begin(), s.end(), h);

            BOOST_REQUIRE(ok);
            BOOST_TEST(h.name == "media");
            BOOST_TEST(h.inverse);
        }

        BOOST_AUTO_TEST_CASE(error1_)
        {
            auto const s = "media=1"sv;
            handler h;

            BOOST_CHECK_THROW(
                black_list_option(s.begin(), s.end(), h),
                parse_error
            );
        }

    BOOST_AUTO_TEST_SUITE_END() // media_option_

    BOOST_AUTO_TEST_SUITE(font_option_)

        BOOST_AUTO_TEST_CASE(ok1_)
        {
            auto const s = "font"sv;
            handler h;

            auto const ok = black_list_option(s.begin(), s.end(), h);

            BOOST_REQUIRE(ok);
            BOOST_TEST(h.name == "font");
            BOOST_TEST(!h.inverse);
        }

        BOOST_AUTO_TEST_CASE(ok2_)
        {
            auto const s = "~font"sv;
            handler h;

            auto const ok = black_list_option(s.begin(), s.end(), h);

            BOOST_REQUIRE(ok);
            BOOST_TEST(h.name == "font");
            BOOST_TEST(h.inverse);
        }

        BOOST_AUTO_TEST_CASE(error1_)
        {
            auto const s = "font=1"sv;
            handler h;

            BOOST_CHECK_THROW(
                black_list_option(s.begin(), s.end(), h),
                parse_error
            );
        }

    BOOST_AUTO_TEST_SUITE_END() // font_option_

    BOOST_AUTO_TEST_SUITE(other_option_)

        BOOST_AUTO_TEST_CASE(ok1_)
        {
            auto const s = "other"sv;
            handler h;

            auto const ok = black_list_option(s.begin(), s.end(), h);

            BOOST_REQUIRE(ok);
            BOOST_TEST(h.name == "other");
            BOOST_TEST(!h.inverse);
        }

        BOOST_AUTO_TEST_CASE(ok2_)
        {
            auto const s = "~other"sv;
            handler h;

            auto const ok = black_list_option(s.begin(), s.end(), h);

            BOOST_REQUIRE(ok);
            BOOST_TEST(h.name == "other");
            BOOST_TEST(h.inverse);
        }

        BOOST_AUTO_TEST_CASE(error1_)
        {
            auto const s = "other=1"sv;
            handler h;

            BOOST_CHECK_THROW(
                black_list_option(s.begin(), s.end(), h),
                parse_error
            );
        }

    BOOST_AUTO_TEST_SUITE_END() // other_option_

    BOOST_AUTO_TEST_SUITE(third_party_option_)

        BOOST_AUTO_TEST_CASE(ok1_)
        {
            auto const s = "third-party"sv;
            handler h;

            auto const ok = black_list_option(s.begin(), s.end(), h);

            BOOST_REQUIRE(ok);
            BOOST_TEST(h.name == "third_party");
            BOOST_TEST(!h.inverse);
        }

        BOOST_AUTO_TEST_CASE(ok2_)
        {
            auto const s = "~third-party"sv;
            handler h;

            auto const ok = black_list_option(s.begin(), s.end(), h);

            BOOST_REQUIRE(ok);
            BOOST_TEST(h.name == "third_party");
            BOOST_TEST(h.inverse);
        }

        BOOST_AUTO_TEST_CASE(error1_)
        {
            auto const s = "third-party=1"sv;
            handler h;

            BOOST_CHECK_THROW(
                black_list_option(s.begin(), s.end(), h),
                parse_error
            );
        }

    BOOST_AUTO_TEST_SUITE_END() // third_party_option_

    BOOST_AUTO_TEST_SUITE(match_case_option_)

        BOOST_AUTO_TEST_CASE(ok1_)
        {
            auto const s = "match-case"sv;
            handler h;

            auto const ok = black_list_option(s.begin(), s.end(), h);

            BOOST_REQUIRE(ok);
            BOOST_TEST(h.name == "match_case");
        }

        BOOST_AUTO_TEST_CASE(error1_)
        {
            auto const s = "~match-case"sv;
            handler h;

            BOOST_CHECK_THROW(
                black_list_option(s.begin(), s.end(), h),
                parse_error
            );
        }

        BOOST_AUTO_TEST_CASE(error2_)
        {
            auto const s = "match-case=1"sv;
            handler h;

            BOOST_CHECK_THROW(
                black_list_option(s.begin(), s.end(), h),
                parse_error
            );
        }

    BOOST_AUTO_TEST_SUITE_END() // match_case_option_

    BOOST_AUTO_TEST_SUITE(collapse_option_)

        BOOST_AUTO_TEST_CASE(ok1_)
        {
            auto const s = "collapse"sv;
            handler h;

            auto const ok = black_list_option(s.begin(), s.end(), h);

            BOOST_REQUIRE(ok);
            BOOST_TEST(h.name == "collapse");
            BOOST_TEST(!h.inverse);
        }

        BOOST_AUTO_TEST_CASE(ok2_)
        {
            auto const s = "~collapse"sv;
            handler h;

            auto const ok = black_list_option(s.begin(), s.end(), h);

            BOOST_REQUIRE(ok);
            BOOST_TEST(h.name == "collapse");
            BOOST_TEST(h.inverse);
        }

        BOOST_AUTO_TEST_CASE(error1_)
        {
            auto const s = "collapse=1"sv;
            handler h;

            BOOST_CHECK_THROW(
                black_list_option(s.begin(), s.end(), h),
                parse_error
            );
        }

    BOOST_AUTO_TEST_SUITE_END() // collapse_option_

    BOOST_AUTO_TEST_SUITE(do_not_track_option_)

        BOOST_AUTO_TEST_CASE(ok1_)
        {
            auto const s = "donottrack"sv;
            handler h;

            auto const ok = black_list_option(s.begin(), s.end(), h);

            BOOST_REQUIRE(ok);
            BOOST_TEST(h.name == "do_not_track");
        }

        BOOST_AUTO_TEST_CASE(error1_)
        {
            auto const s = "~donottrack"sv;
            handler h;

            BOOST_CHECK_THROW(
                black_list_option(s.begin(), s.end(), h),
                parse_error
            );
        }

        BOOST_AUTO_TEST_CASE(error2_)
        {
            auto const s = "donottrack=1"sv;
            handler h;

            BOOST_CHECK_THROW(
                black_list_option(s.begin(), s.end(), h),
                parse_error
            );
        }

    BOOST_AUTO_TEST_SUITE_END() // do_not_track_option_

    BOOST_AUTO_TEST_SUITE(domain_option_)

        BOOST_AUTO_TEST_CASE(ok1_)
        {
            auto const s = "domain=aol.com"sv;
            handler h;

            auto const ok = black_list_option(s.begin(), s.end(), h);

            BOOST_REQUIRE(ok);
            BOOST_TEST(h.name == "domain");
            BOOST_TEST(h.domains.size() == 1);
            BOOST_TEST(h.domains[0] == "aol.com");
        }

        BOOST_AUTO_TEST_CASE(ok2_)
        {
            auto const s = "domain=aol.com|myspace.com"sv;
            handler h;

            auto const ok = black_list_option(s.begin(), s.end(), h);

            BOOST_REQUIRE(ok);
            BOOST_TEST(h.name == "domain");
            BOOST_TEST(h.domains.size() == 2);
            BOOST_TEST(h.domains[0] == "aol.com");
            BOOST_TEST(h.domains[1] == "myspace.com");
        }

        BOOST_AUTO_TEST_CASE(error1_)
        {
            auto const s = "domain"sv;
            handler h;

            BOOST_CHECK_THROW(
                black_list_option(s.begin(), s.end(), h),
                parse_error
            );
        }

        BOOST_AUTO_TEST_CASE(error2_)
        {
            auto const s = "~domain"sv;
            handler h;

            BOOST_CHECK_THROW(
                black_list_option(s.begin(), s.end(), h),
                parse_error
            );
        }

        BOOST_AUTO_TEST_CASE(error3_)
        {
            auto const s = "domain="sv;
            handler h;

            BOOST_CHECK_THROW(
                black_list_option(s.begin(), s.end(), h),
                parse_error
            );
        }

        BOOST_AUTO_TEST_CASE(error4_)
        {
            auto const s = "domain=|"sv;
            handler h;

            BOOST_CHECK_THROW(
                black_list_option(s.begin(), s.end(), h),
                parse_error
            );
        }

        BOOST_AUTO_TEST_CASE(error5_)
        {
            auto const s = "domain=aol.com|"sv;
            handler h;

            BOOST_CHECK_THROW(
                black_list_option(s.begin(), s.end(), h),
                parse_error
            );
        }

        BOOST_AUTO_TEST_CASE(error6_)
        {
            auto const s = "domain=|aol.com"sv;
            handler h;

            BOOST_CHECK_THROW(
                black_list_option(s.begin(), s.end(), h),
                parse_error
            );
        }

        BOOST_AUTO_TEST_CASE(error7_)
        {
            auto const s = "domain=|aol.com|"sv;
            handler h;

            BOOST_CHECK_THROW(
                black_list_option(s.begin(), s.end(), h),
                parse_error
            );
        }

        BOOST_AUTO_TEST_CASE(error8_)
        {
            auto const s = "domain=||"sv;
            handler h;

            BOOST_CHECK_THROW(
                black_list_option(s.begin(), s.end(), h),
                parse_error
            );
        }

    BOOST_AUTO_TEST_SUITE_END() // domain_option_

    BOOST_AUTO_TEST_SUITE(site_key_option_)

        BOOST_AUTO_TEST_CASE(ok1_)
        {
            auto const s = "sitekey=value"sv;
            handler h;

            auto const ok = black_list_option(s.begin(), s.end(), h);

            BOOST_REQUIRE(ok);
            BOOST_TEST(h.name == "site_key");
            BOOST_TEST(h.value == "value");
        }

        BOOST_AUTO_TEST_CASE(error1_)
        {
            auto const s = "sitekey"sv;
            handler h;

            BOOST_CHECK_THROW(
                black_list_option(s.begin(), s.end(), h),
                parse_error
            );
        }

        BOOST_AUTO_TEST_CASE(error2_)
        {
            auto const s = "~sitekey"sv;
            handler h;

            BOOST_CHECK_THROW(
                black_list_option(s.begin(), s.end(), h),
                parse_error
            );
        }

    BOOST_AUTO_TEST_SUITE_END() // site_key_option_

    BOOST_AUTO_TEST_SUITE(csp_option_)

        BOOST_AUTO_TEST_CASE(ok1_)
        {
            auto const s = "csp=value"sv;
            handler h;

            auto const ok = black_list_option(s.begin(), s.end(), h);

            BOOST_REQUIRE(ok);
            BOOST_TEST(h.name == "csp");
            BOOST_TEST(h.value == "value");
        }

        BOOST_AUTO_TEST_CASE(ok2_)
        {
            auto const s = "csp"sv;
            handler h;

            auto const ok = black_list_option(s.begin(), s.end(), h, true);

            BOOST_REQUIRE(ok);
            BOOST_TEST(h.name == "csp");
            BOOST_TEST(h.value.empty());
        }

        BOOST_AUTO_TEST_CASE(error1_)
        {
            auto const s = "csp"sv;
            handler h;

            BOOST_CHECK_THROW(
                black_list_option(s.begin(), s.end(), h),
                parse_error
            );
        }

        BOOST_AUTO_TEST_CASE(error2_)
        {
            auto const s = "~csp"sv;
            handler h;

            BOOST_CHECK_THROW(
                black_list_option(s.begin(), s.end(), h),
                parse_error
            );
        }

    BOOST_AUTO_TEST_SUITE_END() // csp_option_

    BOOST_AUTO_TEST_SUITE(rewrite_option_)

        BOOST_AUTO_TEST_CASE(ok1_)
        {
            auto const s = "rewrite=value"sv;
            handler h;

            auto const ok = black_list_option(s.begin(), s.end(), h);

            BOOST_REQUIRE(ok);
            BOOST_TEST(h.name == "rewrite");
            BOOST_TEST(h.value == "value");
        }

        BOOST_AUTO_TEST_CASE(ok2_)
        {
            auto const s = "rewrite = value"sv;
            handler h;

            auto const ok = black_list_option(s.begin(), s.end(), h);

            BOOST_REQUIRE(ok);
            BOOST_TEST(h.name == "rewrite");
            BOOST_TEST(h.value == "value");
        }

        BOOST_AUTO_TEST_CASE(ok3_)
        {
            auto const s = "rewrite="sv;
            handler h;

            auto const ok = black_list_option(s.begin(), s.end(), h);

            BOOST_REQUIRE(ok);
            BOOST_TEST(h.name == "rewrite");
            BOOST_TEST(h.value == "");
        }

        BOOST_AUTO_TEST_CASE(error1_)
        {
            auto const s = "rewrite"sv;
            handler h;

            BOOST_CHECK_THROW(
                black_list_option(s.begin(), s.end(), h),
                parse_error
            );
        }

        BOOST_AUTO_TEST_CASE(error2_)
        {
            auto const s = "~rewrite"sv;
            handler h;

            BOOST_CHECK_THROW(
                black_list_option(s.begin(), s.end(), h),
                parse_error
            );
        }

    BOOST_AUTO_TEST_SUITE_END() // rewrite_option_

    BOOST_AUTO_TEST_SUITE(invalid_option_)

        BOOST_AUTO_TEST_CASE(error1_)
        {
            auto const s = ""sv;
            handler h;

            BOOST_CHECK_THROW(
                black_list_option(s.begin(), s.end(), h),
                parse_error
            );
        }

        BOOST_AUTO_TEST_CASE(error2_)
        {
            auto const s = "foo"sv;
            handler h;

            auto const ok = black_list_option(s.begin(), s.end(), h);
            BOOST_REQUIRE(!ok);
        }

        BOOST_AUTO_TEST_CASE(error3_)
        {
            auto const s = "~"sv;
            handler h;

            BOOST_CHECK(!black_list_option(s.begin(), s.end(), h));
        }

        BOOST_AUTO_TEST_CASE(error4_)
        {
            auto const s = "~~script"sv;
            handler h;

            BOOST_CHECK(!black_list_option(s.begin(), s.end(), h));
        }

    BOOST_AUTO_TEST_SUITE_END() // invalid_option_

    BOOST_AUTO_TEST_SUITE(basic_filter_options_)

        BOOST_AUTO_TEST_CASE(ok1_)
        {
            auto const s = "$script"sv;
            handler h;

            basic_filter_options(s.begin(), s.end(), h);

            BOOST_REQUIRE(h.options.size() == 1);
            BOOST_TEST(h.options[0] == "script");
        }

        BOOST_AUTO_TEST_CASE(ok2_)
        {
            auto const s = "$  script , ~image\t\t\t"sv;
            handler h;

            basic_filter_options(s.begin(), s.end(), h);

            BOOST_REQUIRE(h.options.size() == 2);
            BOOST_TEST(h.options[0] == "script");
            BOOST_TEST(h.options[1] == "image");
        }

        BOOST_AUTO_TEST_CASE(error1_)
        {
            auto const s = "$"sv;
            handler h;

            BOOST_CHECK_THROW(
                basic_filter_options(s.begin(), s.end(), h),
                parse_error
            );
        }

        BOOST_AUTO_TEST_CASE(error2_)
        {
            auto const s = "$script,"sv;
            handler h;

            BOOST_CHECK_THROW(
                basic_filter_options(s.begin(), s.end(), h),
                parse_error
            );
        }

        BOOST_AUTO_TEST_CASE(error3_)
        {
            auto const s = "$,script"sv;
            handler h;

            BOOST_CHECK_THROW(
                basic_filter_options(s.begin(), s.end(), h),
                parse_error
            );
        }

        BOOST_AUTO_TEST_CASE(error4_)
        {
            auto const s = "$foo"sv;
            handler h;

            BOOST_CHECK_THROW(
                basic_filter_options(s.begin(), s.end(), h),
                parse_error
            );
        }

        BOOST_AUTO_TEST_CASE(error5_)
        {
            auto const s = "$document"sv;
            handler h;

            BOOST_CHECK_THROW(
                basic_filter_options(s.begin(), s.end(), h),
                parse_error
            );
        }

    BOOST_AUTO_TEST_SUITE_END() // basic_filter_options_

    BOOST_AUTO_TEST_SUITE(exception_filter_options_)

        BOOST_AUTO_TEST_CASE(ok1_)
        {
            auto const s = "$document"sv;
            handler h;

            exception_filter_options(s.begin(), s.end(), h);

            BOOST_REQUIRE(h.options.size() == 1);
            BOOST_TEST(h.options[0] == "document");
        }

        BOOST_AUTO_TEST_CASE(ok2_)
        {
            auto const s = "$script,~elemhide"sv;
            handler h;

            exception_filter_options(s.begin(), s.end(), h);

            BOOST_REQUIRE(h.options.size() == 2);
            BOOST_TEST(h.options[0] == "script");
            BOOST_TEST(h.options[1] == "elemhide");
        }

        BOOST_AUTO_TEST_CASE(ok3_)
        {
            auto const s = "$csp"sv;
            handler h;

            exception_filter_options(s.begin(), s.end(), h);

            BOOST_REQUIRE(h.options.size() == 1);
            BOOST_TEST(h.options[0] == "csp");
        }

        BOOST_AUTO_TEST_CASE(error1_)
        {
            auto const s = "$foo"sv;
            handler h;

            BOOST_CHECK_THROW(
                basic_filter_options(s.begin(), s.end(), h),
                parse_error
            );
        }

    BOOST_AUTO_TEST_SUITE_END() // exception_filter_options_

    BOOST_AUTO_TEST_SUITE(scan_filter_options_)

        BOOST_AUTO_TEST_CASE(ok1_)
        {
            auto const s = "~match-case"sv;
            auto it = s.begin();

            auto const ok = scan_filter_options(it, s.end());

            BOOST_REQUIRE(ok);
            BOOST_TEST((it - s.begin()) == 11);
        }

        BOOST_AUTO_TEST_CASE(ok2_)
        {
            auto const s = "script"sv;
            auto it = s.begin();

            auto const ok = scan_filter_options(it, s.end());

            BOOST_REQUIRE(ok);
            BOOST_TEST((it - s.begin()) == 6);
        }

        BOOST_AUTO_TEST_CASE(ok3_)
        {
            auto const s = "SITEKEY=0ABD"sv;
            auto it = s.begin();

            auto const ok = scan_filter_options(it, s.end());

            BOOST_REQUIRE(ok);
            BOOST_TEST((it - s.begin()) == 12);
        }

        BOOST_AUTO_TEST_CASE(ok4_)
        {
            auto const s = "script,~image,domain=aol.com|myspace.com"sv;
            auto it = s.begin();

            auto const ok = scan_filter_options(it, s.end());

            BOOST_REQUIRE(ok);
            BOOST_TEST((it - s.begin()) == 40);
        }

        BOOST_AUTO_TEST_CASE(ok5_)
        {
            auto const s = "/[a-z$]+$/$image"sv;
            auto it = s.begin() + 11;
            BOOST_REQUIRE(*it == 'i');

            auto const ok = scan_filter_options(it, s.end());

            BOOST_REQUIRE(ok);
            BOOST_TEST((it - s.begin()) == 16);
        }

        BOOST_AUTO_TEST_CASE(ok6_)
        {
            auto const s = "/[$image"sv;
            auto it = s.begin() + 3;
            BOOST_REQUIRE(*it == 'i');

            auto const ok = scan_filter_options(it, s.end());

            BOOST_REQUIRE(ok);
            BOOST_TEST((it - s.begin()) == 8);
        }

        BOOST_AUTO_TEST_CASE(ok7_)
        {
            auto const s = " script\t, ~image  "sv;
            auto it = s.begin();

            auto const ok = scan_filter_options(it, s.end());

            BOOST_REQUIRE(ok);
            BOOST_TEST((it - s.begin()) == 18);
        }

        BOOST_AUTO_TEST_CASE(error1_)
        {
            auto const s = "/[a-z$]+$/$image"sv;
            auto it = s.begin() + 6;
            BOOST_REQUIRE(*it == ']');

            auto const ok = scan_filter_options(it, s.end());

            BOOST_REQUIRE(!ok);
        }

        BOOST_AUTO_TEST_CASE(error2_)
        {
            auto const s = "/[a-z$]+$/$image"sv;
            auto it = s.begin() + 9;
            BOOST_REQUIRE(*it == '/');

            auto const ok = scan_filter_options(it, s.end());

            BOOST_REQUIRE(!ok);
        }

        BOOST_AUTO_TEST_CASE(error3_)
        {
            auto const s = "/[$image]+/"sv;
            auto it = s.begin() + 3;
            BOOST_REQUIRE(*it == 'i');

            auto const ok = scan_filter_options(it, s.end());

            BOOST_REQUIRE(!ok);
        }

        BOOST_AUTO_TEST_CASE(error4_)
        {
            auto const s = "aol.com@image,"sv;
            auto it = s.begin() + 8;
            BOOST_REQUIRE(*it == 'i');

            auto const ok = scan_filter_options(it, s.end());

            BOOST_REQUIRE(!ok);
        }

    BOOST_AUTO_TEST_SUITE_END() // scan_filter_options_

BOOST_AUTO_TEST_SUITE_END() // filter_option_

} // namespace adblock_parser::testing
