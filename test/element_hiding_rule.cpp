#include "src/element_hiding_rule.hpp"

#include <adblock_parser/char_type.hpp>
#include <adblock_parser/event_handler.hpp>

#include "src/error.hpp"
#include "src/utility.hpp"

#include <string_view>
#include <vector>

#include <boost/test/unit_test.hpp>

namespace adblock_parser::testing {

using namespace std::literals;

BOOST_AUTO_TEST_SUITE(element_hiding_rule_)

    struct handler : event_handler
    {
        void
        begin_filter_domains(iterator /*begin*/, iterator /*end*/) override
        {
            this->domains = {};
        }

        void
        end_filter_domains(iterator /*begin*/, iterator /*end*/) override
        {
            this->got_domains = true;
        }

        void
        filter_domain(iterator begin, iterator end) override
        {
            this->domains.push_back(make_string_view(begin, end));
        }

        void
        css_selector(iterator begin, iterator end) override
        {
            selector = make_string_view(begin, end);
        }

        void
        error(iterator /*begin*/, iterator /*end*/, std::string_view /*msg*/) override
        {
            this->got_error = true;
        }

        std::vector<std::string_view> domains;
        std::string_view selector;
        bool got_domains = false;
        bool got_error = false;
    };

    BOOST_AUTO_TEST_SUITE(basic_element_hiding_rule_)

        BOOST_AUTO_TEST_CASE(ok1_)
        {
            auto const s = "###ad"sv;
            handler h;

            auto const ok =
                basic_element_hiding_rule(s.begin(), s.begin(), s.end(), h);

            BOOST_REQUIRE(ok);
            BOOST_TEST(!h.got_domains);
            BOOST_TEST(h.domains.empty());
            BOOST_TEST(h.selector == "#ad");
            BOOST_TEST(!h.got_error);
        }

        BOOST_AUTO_TEST_CASE(ok2_)
        {
            auto const s = "aol.com###ad"sv;
            handler h;

            auto const ok = basic_element_hiding_rule(
                        s.begin(), s.begin() + 7, s.end(), h);

            BOOST_REQUIRE(ok);
            BOOST_TEST(h.got_domains);
            BOOST_TEST(h.domains.size() == 1);
            BOOST_TEST(h.domains[0] == "aol.com");
            BOOST_TEST(h.selector == "#ad");
            BOOST_TEST(!h.got_error);
        }

        BOOST_AUTO_TEST_CASE(ok3_)
        {
            auto const s = "aol.com,~myspace.com###ad"sv;
            handler h;

            auto const ok = basic_element_hiding_rule(
                        s.begin(), s.begin() + 20, s.end(), h);

            BOOST_REQUIRE(ok);
            BOOST_TEST(h.got_domains);
            BOOST_TEST(h.domains.size() == 2);
            BOOST_TEST(h.domains[0] == "aol.com");
            BOOST_TEST(h.domains[1] == "~myspace.com");
            BOOST_TEST(h.selector == "#ad");
            BOOST_TEST(!h.got_error);
        }

        BOOST_AUTO_TEST_CASE(error1_)
        {
            auto const s = "##"sv;
            handler h;

            auto const ok =
                basic_element_hiding_rule(s.begin(), s.begin(), s.end(), h);

            BOOST_REQUIRE(!ok);
            BOOST_TEST(!h.got_domains);
            BOOST_TEST(h.domains.empty());
            BOOST_TEST(h.got_error);
        }

        BOOST_AUTO_TEST_CASE(error2_)
        {
            auto const s = ",###ad"sv;
            handler h;

            auto const ok =
                basic_element_hiding_rule(s.begin(), s.begin() + 1, s.end(), h);

            BOOST_REQUIRE(!ok);
            BOOST_TEST(!h.got_domains);
            BOOST_TEST(h.domains.empty());
            BOOST_TEST(h.got_error);
        }

        BOOST_AUTO_TEST_CASE(error3_)
        {
            auto const s = "aol.com##"sv;
            handler h;

            auto const ok =
                basic_element_hiding_rule(s.begin(), s.begin() + 7, s.end(), h);

            BOOST_REQUIRE(!ok);
            BOOST_TEST(h.got_domains);
            BOOST_TEST(h.domains.size() == 1);
            BOOST_TEST(h.got_error);
        }

    BOOST_AUTO_TEST_SUITE_END() // basic_element_hiding_rule_

    BOOST_AUTO_TEST_SUITE(exception_element_hiding_rule_)

        BOOST_AUTO_TEST_CASE(ok1_)
        {
            auto const s = "#@##ad"sv;
            handler h;

            auto const ok =
                exception_element_hiding_rule(s.begin(), s.begin(), s.end(), h);

            BOOST_REQUIRE(ok);
            BOOST_TEST(!h.got_domains);
            BOOST_TEST(h.domains.empty());
            BOOST_TEST(h.selector == "#ad");
            BOOST_TEST(!h.got_error);
        }

        BOOST_AUTO_TEST_CASE(ok2_)
        {
            auto const s = "aol.com#@##ad"sv;
            handler h;

            auto const ok = exception_element_hiding_rule(
                        s.begin(), s.begin() + 7, s.end(), h);

            BOOST_REQUIRE(ok);
            BOOST_TEST(h.got_domains);
            BOOST_TEST(h.domains.size() == 1);
            BOOST_TEST(h.domains[0] == "aol.com");
            BOOST_TEST(h.selector == "#ad");
            BOOST_TEST(!h.got_error);
        }

        BOOST_AUTO_TEST_CASE(ok3_)
        {
            auto const s = "aol.com,~myspace.com#@##ad"sv;
            handler h;

            auto const ok = exception_element_hiding_rule(
                        s.begin(), s.begin() + 20, s.end(), h);

            BOOST_REQUIRE(ok);
            BOOST_TEST(h.got_domains);
            BOOST_TEST(h.domains.size() == 2);
            BOOST_TEST(h.domains[0] == "aol.com");
            BOOST_TEST(h.domains[1] == "~myspace.com");
            BOOST_TEST(h.selector == "#ad");
            BOOST_TEST(!h.got_error);
        }

        BOOST_AUTO_TEST_CASE(error1_)
        {
            auto const s = "#@#"sv;
            handler h;

            auto const ok =
                exception_element_hiding_rule(s.begin(), s.begin(), s.end(), h);

            BOOST_REQUIRE(!ok);
            BOOST_TEST(!h.got_domains);
            BOOST_TEST(h.domains.empty());
            BOOST_TEST(h.got_error);
        }

        BOOST_AUTO_TEST_CASE(error2_)
        {
            auto const s = ",#@##ad"sv;
            handler h;

            auto const ok =
                exception_element_hiding_rule(s.begin(), s.begin() + 1, s.end(), h);

            BOOST_REQUIRE(!ok);
            BOOST_TEST(!h.got_domains);
            BOOST_TEST(h.domains.empty());
            BOOST_TEST(h.got_error);
        }

        BOOST_AUTO_TEST_CASE(error3_)
        {
            auto const s = "aol.com#@#"sv;
            handler h;

            auto const ok =
                exception_element_hiding_rule(s.begin(), s.begin() + 7, s.end(), h);

            BOOST_REQUIRE(!ok);
            BOOST_TEST(h.got_domains);
            BOOST_TEST(h.domains.size() == 1);
            BOOST_TEST(h.got_error);
        }

    BOOST_AUTO_TEST_SUITE_END() // exception_element_hiding_rule_

    BOOST_AUTO_TEST_SUITE(extended_element_hiding_rule_)

        BOOST_AUTO_TEST_CASE(ok1_)
        {
            auto const s = "#?##ad"sv;
            handler h;

            auto const ok = extended_element_hiding_rule(s.begin(), s.begin(), s.end(), h);

            BOOST_REQUIRE(ok);
            BOOST_TEST(!h.got_domains);
            BOOST_TEST(h.domains.empty());
            BOOST_TEST(h.selector == "#ad");
            BOOST_TEST(!h.got_error);
        }

        BOOST_AUTO_TEST_CASE(ok2_)
        {
            auto const s = "aol.com#?##ad"sv;
            handler h;

            auto const ok = extended_element_hiding_rule(
                        s.begin(), s.begin() + 7, s.end(), h);

            BOOST_REQUIRE(ok);
            BOOST_TEST(h.got_domains);
            BOOST_TEST(h.domains.size() == 1);
            BOOST_TEST(h.domains[0] == "aol.com");
            BOOST_TEST(h.selector == "#ad");
            BOOST_TEST(!h.got_error);
        }

        BOOST_AUTO_TEST_CASE(ok3_)
        {
            auto const s = "aol.com,~myspace.com#?##ad"sv;
            handler h;

            auto const ok = extended_element_hiding_rule(
                        s.begin(), s.begin() + 20, s.end(), h);

            BOOST_REQUIRE(ok);
            BOOST_TEST(h.got_domains);
            BOOST_TEST(h.domains.size() == 2);
            BOOST_TEST(h.domains[0] == "aol.com");
            BOOST_TEST(h.domains[1] == "~myspace.com");
            BOOST_TEST(h.selector == "#ad");
            BOOST_TEST(!h.got_error);
        }

        BOOST_AUTO_TEST_CASE(error1_)
        {
            auto const s = "#?#"sv;
            handler h;

            auto const ok =
                extended_element_hiding_rule(s.begin(), s.begin(), s.end(), h);

            BOOST_REQUIRE(!ok);
            BOOST_TEST(!h.got_domains);
            BOOST_TEST(h.domains.empty());
            BOOST_TEST(h.got_error);
        }

        BOOST_AUTO_TEST_CASE(error2_)
        {
            auto const s = ",#?##ad"sv;
            handler h;

            auto const ok =
                extended_element_hiding_rule(s.begin(), s.begin() + 1, s.end(), h);

            BOOST_REQUIRE(!ok);
            BOOST_TEST(!h.got_domains);
            BOOST_TEST(h.domains.empty());
            BOOST_TEST(h.got_error);
        }

        BOOST_AUTO_TEST_CASE(error3_)
        {
            auto const s = "aol.com#?#"sv;
            handler h;

            auto const ok =
                extended_element_hiding_rule(s.begin(), s.begin() + 7, s.end(), h);

            BOOST_REQUIRE(!ok);
            BOOST_TEST(h.got_domains);
            BOOST_TEST(h.domains.size() == 1);
            BOOST_TEST(h.got_error);
        }

    BOOST_AUTO_TEST_SUITE_END() // extended_element_hiding_rule_

    BOOST_AUTO_TEST_SUITE(snippet_filter_rule_)

        struct handler : event_handler
        {
            void
            begin_filter_domains(iterator /*begin*/, iterator /*end*/) override
            {
                this->domains = {};
            }

            void
            end_filter_domains(iterator /*begin*/, iterator /*end*/) override
            {
                this->got_domains = true;
            }

            void
            filter_domain(iterator begin, iterator end) override
            {
                this->domains.push_back(make_string_view(begin, end));
            }

            void
            snippet(iterator begin, iterator end) override
            {
                this->body = make_string_view(begin, end);
            }

            void
            error(iterator /*begin*/, iterator /*end*/, std::string_view /*msg*/) override
            {
                this->got_error = true;
            }

            std::vector<std::string_view> domains;
            std::string_view body;
            bool got_domains = false;
            bool got_error = false;
        };

        BOOST_AUTO_TEST_CASE(ok1_)
        {
            auto const s = "aol.com#$#snippet"sv;
            handler h;

            auto const ok =
                snippet_filter_rule(s.begin(), s.begin() + 7, s.end(), h);

            BOOST_REQUIRE(ok);
            BOOST_TEST(h.got_domains);
            BOOST_TEST(h.domains.size() == 1);
            BOOST_TEST(h.domains[0] == "aol.com");
            BOOST_TEST(h.body == "snippet");
            BOOST_TEST(!h.got_error);
        }

    BOOST_AUTO_TEST_SUITE_END() // snippet_filter_rule_

    BOOST_AUTO_TEST_SUITE(filter_domain_)

        struct handler : event_handler
        {
            void
            filter_domain(iterator begin, iterator end) override
            {
                domain = make_string_view(begin, end);
            }

            std::string_view domain;
        };

        BOOST_AUTO_TEST_CASE(ok1_)
        {
            auto const s = "  aol.com\t\t"sv;
            handler h;

            filter_domain(s.begin(), s.end(), h);

            BOOST_TEST(h.domain == "aol.com");
        }

        BOOST_AUTO_TEST_CASE(error1_)
        {
            auto const s = ""sv;
            handler h;

            BOOST_CHECK_THROW(
                filter_domain(s.begin(), s.end(), h),
                parse_error
            );
        }

    BOOST_AUTO_TEST_SUITE_END() // filter_domain_

    BOOST_AUTO_TEST_SUITE(filter_domains_)

        struct handler : event_handler
        {
            void
            begin_filter_domains(iterator /*begin*/, iterator /*end*/) override
            {
                this->domains = {};
            }

            void
            end_filter_domains(iterator /*begin*/, iterator /*end*/) override
            {
                this->got_domains = true;
            }

            void
            filter_domain(iterator begin, iterator end) override
            {
                this->domains.push_back(make_string_view(begin, end));
            }

            void
            error(iterator /*begin*/, iterator /*end*/, std::string_view /*msg*/) override
            {
                this->got_error = true;
            }

            std::vector<std::string_view> domains;
            bool got_domains = false;
            bool got_error = false;
        };

        BOOST_AUTO_TEST_CASE(ok1_)
        {
            auto const s = ""sv;
            handler h;

            filter_domains(s.begin(), s.end(), h);

            BOOST_REQUIRE(!h.got_domains);
            BOOST_REQUIRE(!h.got_error);
            BOOST_REQUIRE(h.domains.size() == 0);
        }

        BOOST_AUTO_TEST_CASE(ok2_)
        {
            auto const s = "aol.com"sv;
            handler h;

            filter_domains(s.begin(), s.end(), h);

            BOOST_REQUIRE(h.got_domains);
            BOOST_REQUIRE(!h.got_error);
            BOOST_REQUIRE(h.domains.size() == 1);
            BOOST_REQUIRE(h.domains[0] == "aol.com");
        }

        BOOST_AUTO_TEST_CASE(ok3_)
        {
            auto const s = "  aol.com , ~myspace.com  "sv;
            handler h;

            filter_domains(s.begin(), s.end(), h);

            BOOST_REQUIRE(h.got_domains);
            BOOST_REQUIRE(!h.got_error);
            BOOST_REQUIRE(h.domains.size() == 2);
            BOOST_REQUIRE(h.domains[0] == "aol.com");
            BOOST_REQUIRE(h.domains[1] == "~myspace.com");
        }

        BOOST_AUTO_TEST_CASE(error1_)
        {
            auto const s = ","sv;
            handler h;

            BOOST_CHECK_THROW(
                filter_domains(s.begin(), s.end(), h),
                parse_error
            );
        }

        BOOST_AUTO_TEST_CASE(error2_)
        {
            auto const s = ",aol.com"sv;
            handler h;

            BOOST_CHECK_THROW(
                filter_domains(s.begin(), s.end(), h),
                parse_error
            );
        }

        BOOST_AUTO_TEST_CASE(error3_)
        {
            auto const s = "aol.com,"sv;
            handler h;

            BOOST_CHECK_THROW(
                filter_domains(s.begin(), s.end(), h),
                parse_error
            );
        }

    BOOST_AUTO_TEST_SUITE_END() // filter_domains_

    BOOST_AUTO_TEST_SUITE(css_selector_)

        struct handler : event_handler
        {
            void
            css_selector(iterator begin, iterator end) override
            {
                selector = make_string_view(begin, end);
            }

            std::string_view selector;
        };

        BOOST_AUTO_TEST_CASE(ok1_)
        {
            auto const s = "#ad"sv;
            handler h;

            css_selector(s.begin(), s.end(), h);

            BOOST_TEST(h.selector == "#ad");
        }

        BOOST_AUTO_TEST_CASE(error1_)
        {
            auto const s = ""sv;
            handler h;

            BOOST_CHECK_THROW(
                css_selector(s.begin(), s.end(), h),
                parse_error
            );
        }

    BOOST_AUTO_TEST_SUITE_END() // css_selector_

BOOST_AUTO_TEST_SUITE_END() // element_hiding_rule_

} // namespace adblock_parser::testing
