#include <adblock_parser/json.hpp>

#include <string>
#include <iostream>

#include <boost/test/unit_test.hpp>

namespace adblock_parser::testing {

BOOST_AUTO_TEST_SUITE(json_)

BOOST_AUTO_TEST_SUITE(from_json_)

    BOOST_AUTO_TEST_CASE(ok1_)
    {
        auto const json = R"([
            {
                "type": "basic filter rule",
                "pattern": {
                    "type": "domain",
                    "pattern": "foo.com"
                }
            },
            {
                "type": "basic filter rule",
                "pattern": {
                    "type": "regex",
                    "pattern": "regex"
                }
            },
            {
                "type": "basic filter rule",
                "pattern": {
                    "type": "basic",
                    "pattern": "foo.com",
                    "prefix" : true
                },
                "options": [
                    { "type": "script", "inverse": true },
                    { "type": "image" },
                    { "type": "domain", "domains": [ "aol.com", "myspace.com" ] },
                    { "type": "sitekey", "value": "key" }
                ]
            }
        ])";
        auto const expected =
            "[Adblock Plus 2.0]\n"
            "||foo.com\n"
            "/regex/\n"
            "|foo.com$~script,image,domain=aol.com|myspace.com,sitekey=key\n"
            ;

        auto const s = from_json(json::parse(json).get_array());
        BOOST_TEST(s == expected);
    }

    BOOST_AUTO_TEST_CASE(ok2_)
    {
        auto const json = R"([
            {
                "type": "basic element hiding rule",
                "selector": "#ad"
            },
            {
                "type": "basic element hiding rule",
                "selector": "#ad",
                "domains": [ "aol.com", "myspace.com" ]
            }
        ])";

        auto const expected =
            "[Adblock Plus 2.0]\n"
            "###ad\n"
            "aol.com,myspace.com###ad\n"
            ;

        auto const s = from_json(json::parse(json).get_array());
        BOOST_TEST(s == expected);
    }

    BOOST_AUTO_TEST_CASE(ok3_)
    {
        auto const json = R"([
            {
                "type": "exception element hiding rule",
                "selector": "#ad"
            },
            {
                "type": "exception element hiding rule",
                "selector": "#ad",
                "domains": [ "aol.com" ]
            }
        ])";

        auto const expected =
            "[Adblock Plus 2.0]\n"
            "#@##ad\n"
            "aol.com#@##ad\n"
            ;

        auto const s = from_json(json::parse(json).get_array());
        BOOST_TEST(s == expected);
    }

    BOOST_AUTO_TEST_CASE(ok4_)
    {
        auto const json = R"([
            {
                "type": "extended element hiding rule",
                "selector": "#ad"
            },
            {
                "type": "extended element hiding rule",
                "selector": "#ad",
                "domains": [ "aol.com" ]
            }
        ])";

        auto const expected =
            "[Adblock Plus 2.0]\n"
            "#?##ad\n"
            "aol.com#?##ad\n"
            ;

        auto const s = from_json(json::parse(json).get_array());
        BOOST_TEST(s == expected);
    }

    BOOST_AUTO_TEST_CASE(ok5_)
    {
        auto const json = R"([
            {
                "type": "snippet filter rule",
                "snippet": "snippet",
                "domains": [ "aol.com" ]
            }
        ])";

        auto const expected =
            "[Adblock Plus 2.0]\n"
            "aol.com#$#snippet\n"
            ;

        auto const s = from_json(json::parse(json).get_array());
        BOOST_TEST(s == expected);
    }

    BOOST_AUTO_TEST_CASE(ok6_)
    {
        auto const json = R"([
            {
                "type": "comment",
                "line": "! comment"
            }
        ])";

        auto const expected =
            "[Adblock Plus 2.0]\n"
            "! comment\n"
            ;

        auto const s = from_json(json::parse(json).get_array());
        BOOST_TEST(s == expected);
    }

BOOST_AUTO_TEST_SUITE_END() // from_json_

BOOST_AUTO_TEST_SUITE_END() // json_

} // namespace adblock_parser::testing
