#include <adblock_parser.hpp>

#include <adblock_parser/json.hpp>

#include "src/utility.hpp"

#include <string_view>
#include <fstream>
#include <sstream>

#define BOOST_TEST_MODULE main
#include <boost/test/unit_test.hpp>

namespace adblock_parser::testing {

BOOST_AUTO_TEST_SUITE(main_)

    BOOST_AUTO_TEST_CASE(comment_)
    {
        auto const s = "[Adblock Plus 2.0]\n"
                       "! comment\n"
                       ;

        auto const expected = R"([
            {
                "type": "comment",
                "line": "! comment"
            }
        ])";

        auto const r = to_json(s);

        BOOST_REQUIRE(r);
        BOOST_TEST(r.rules == json::parse(expected));
    }

    BOOST_AUTO_TEST_CASE(basic_filter_rule_)
    {
        auto const s = "[Adblock Plus 2.0]\n"
                       "||foo.com\n"
                       "/regex/\n"
                       "|foo.com$~script, image\n"
                       ;

        auto const expected = R"([
            {
                "type": "basic filter rule",
                "pattern": {
                    "type": "domain",
                    "pattern": "foo.com"
                }
            },
            {
                "type": "basic filter rule",
                "pattern": {
                    "type": "regex",
                    "pattern": "regex"
                }
            },
            {
                "type": "basic filter rule",
                "pattern": {
                    "type": "basic",
                    "pattern": "foo.com",
                    "prefix" : true
                },
                "options": [
                    { "type": "script", "inverse": true },
                    { "type": "image" }
                ]
            }
        ])";

        auto const r = to_json(s);

        BOOST_REQUIRE(r);
        BOOST_TEST(r.rules == json::parse(expected));
    }

    BOOST_AUTO_TEST_CASE(exception_filter_rule_)
    {
        auto const s = "[Adblock Plus 2.0]\n"
                       "@@abc.gov|\n"
                       "@@abc.gov$script,~image\n"
                       ;

        auto const expected = R"([
            {
                "type": "exception filter rule",
                "pattern": {
                    "type": "basic",
                    "pattern": "abc.gov",
                    "suffix": true
                }
            },
            {
                "type": "exception filter rule",
                "pattern": {
                    "type": "basic",
                    "pattern": "abc.gov"
                },
                "options": [
                    { "type": "script" },
                    { "type": "image", "inverse": true }
                ]
            }
        ])";

        auto const r = to_json(s);

        BOOST_REQUIRE(r);
        BOOST_TEST(r.rules == json::parse(expected));
    }

    BOOST_AUTO_TEST_CASE(basic_element_hiding_rule_)
    {
        auto const s = "[Adblock Plus 2.0]\n"
                       "###ad\n"
                       "aol.com, myspace.com###ad\n"
                       ;

        auto const expected = R"([
            {
                "type": "basic element hiding rule",
                "selector": "#ad"
            },
            {
                "type": "basic element hiding rule",
                "selector": "#ad",
                "domains": [ "aol.com", "myspace.com" ]
            }
        ])";

        auto const r = to_json(s);

        BOOST_REQUIRE(r);
        BOOST_TEST(r.rules == json::parse(expected));
    }

    BOOST_AUTO_TEST_CASE(exception_element_hiding_rule_)
    {
        auto const s = "[Adblock Plus 2.0]\n"
                       "#@##ad\n"
                       "aol.com#@##ad\n"
                       ;

        auto const expected = R"([
            {
                "type": "exception element hiding rule",
                "selector": "#ad"
            },
            {
                "type": "exception element hiding rule",
                "selector": "#ad",
                "domains": [ "aol.com" ]
            }
        ])";

        auto const r = to_json(s);

        BOOST_REQUIRE(r);
        BOOST_TEST(r.rules == json::parse(expected));
    }

    BOOST_AUTO_TEST_CASE(extended_element_hiding_rule_)
    {
        auto const s = "[Adblock Plus 2.0]\n"
                       "#?##ad\n"
                       "aol.com#?##ad\n"
                       ;

        auto const expected = R"([
            {
                "type": "extended element hiding rule",
                "selector": "#ad"
            },
            {
                "type": "extended element hiding rule",
                "selector": "#ad",
                "domains": [ "aol.com" ]
            }
        ])";

        auto const r = to_json(s);

        BOOST_REQUIRE(r);
        BOOST_TEST(r.rules == json::parse(expected));
    }

    BOOST_AUTO_TEST_CASE(empty_line_)
    {
        auto const s = "[Adblock Plus 2.0]\n"
                       "\n\n\n\n"
                       "#?##ad\n"
                       ;

        auto const expected = R"([
            {
                "type": "extended element hiding rule",
                "selector": "#ad"
            }
        ])";

        auto const r = to_json(s);

        BOOST_REQUIRE(r);
        BOOST_TEST(r.rules == json::parse(expected));
    }

    BOOST_AUTO_TEST_CASE(confusing1_)
    {
        auto const s = "[Adblock Plus 2.0]\n"
                       "/foo.jpg$image\n"
                       "/foo.jpg$/$image\n"
                       "@@/foo.jpg$/$image\n"
                       ;

        auto const expected = R"([
            {
                "type": "basic filter rule",
                "pattern": { "type": "basic", "pattern": "/foo.jpg" },
                "options": [ { "type": "image" } ]
            },
            {
                "type": "basic filter rule",
                "pattern": { "type": "regex", "pattern": "foo.jpg$" },
                "options": [ { "type": "image" } ]
            },
            {
                "type": "exception filter rule",
                "pattern": { "type": "regex", "pattern": "foo.jpg$" },
                "options": [ { "type": "image" } ]
            }
        ])";

        auto const r = to_json(s);

        BOOST_REQUIRE(r);
        BOOST_TEST(r.rules == json::parse(expected));
    }

    BOOST_AUTO_TEST_CASE(error1_)
    {
        auto const s = ""
                       ;
        auto const expected = R"([
            {
                "line_no": 1,
                "column": 0,
                "line": "",
                "message": "invalid header line"
            }
        ])";

        auto const r = to_json(s);

        BOOST_REQUIRE(!r);
        BOOST_TEST(r.errors == json::parse(expected));
    }

    BOOST_AUTO_TEST_CASE(error2_)
    {
        auto const s = "! comment\n"
                       ;
        auto const expected = R"([
            {
                "line_no": 1,
                "column": 0,
                "line": "! comment",
                "message": "invalid header line"
            }
        ])";

        auto const r = to_json(s);

        BOOST_REQUIRE(!r);
        BOOST_TEST(r.errors == json::parse(expected));
    }

    BOOST_AUTO_TEST_CASE(error3_)
    {
        auto const s = "[Adblock Plus 2.0]\n"
                       "\n\n"
                       "aol.com$\n"
                       ;
        auto const expected = R"([
            {
                "line_no": 4,
                "column": 8,
                "line": "aol.com$",
                "message": "empty option"
            }
        ])";

        auto const r = to_json(s);

        BOOST_REQUIRE(!r);
        BOOST_TEST(r.errors == json::parse(expected));
    }

    BOOST_AUTO_TEST_SUITE(header_)

        struct header_handler : event_handler
        {
            void
            header(iterator /*bol*/, iterator /*eol*/,
                   iterator begin, iterator end) override
            {
                this->version = make_string_view(begin, end);
            }

            void
            error(iterator /*begin*/, iterator /*end*/,
                  std::string_view msg) override
            {
                this->error_msg = msg;
            }

            std::string_view version;
            std::string_view error_msg;
        };

        BOOST_AUTO_TEST_CASE(ok1_)
        {
            auto const s = "[Adblock Plus 2.0]\n";
            header_handler h;

            auto const ok = parse_filter_list(s, h);

            BOOST_REQUIRE(ok);
            BOOST_TEST(h.version == "2.0");
        }

        BOOST_AUTO_TEST_CASE(ok2_)
        {
            auto const s = "[Adblock Plus  1.5a\t ]";
            header_handler h;

            auto const ok = parse_filter_list(s, h);

            BOOST_REQUIRE(ok);
            BOOST_TEST(h.version == "1.5a");
        }

        BOOST_AUTO_TEST_CASE(error1_)
        {
            auto const s = "[AdBlock Plus 2.0]\n";
            header_handler h;

            auto const ok = parse_filter_list(s, h);

            BOOST_REQUIRE(!ok);
            BOOST_TEST(h.error_msg == "invalid header line");
        }

    BOOST_AUTO_TEST_SUITE_END() // header_

#if 0
    BOOST_AUTO_TEST_CASE(easylist_)
    {
        std::filebuf buf;
        buf.open("/home/stream/.adblock/easylist.txt", std::ios_base::in);
        std::ostringstream oss;

        oss << &buf;

        auto const s = oss.str();
        auto const r = to_json(s);

        BOOST_TEST(r);

        for (auto& e: r.errors) {
            std::cout << e << "\n";
        }
    }
#endif

BOOST_AUTO_TEST_SUITE_END() // main_

} // namespace adblock_parser::testing

