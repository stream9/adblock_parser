#include "src/address_pattern.hpp"

#include <adblock_parser/char_type.hpp>
#include <adblock_parser/event_handler.hpp>

#include "src/error.hpp"
#include "src/utility.hpp"

#include <boost/test/unit_test.hpp>

namespace adblock_parser::testing {

using namespace std::literals;

BOOST_AUTO_TEST_SUITE(address_pattern_)

    struct handler : public event_handler
    {
        void
        basic_address_pattern(iterator begin, iterator end,
                             bool prefix, bool suffix) override
        {
            this->got_basic_address_pattern = true;
            this->pattern = make_string_view(begin, end);
            this->prefix_match = prefix;
            this->suffix_match = suffix;
        }

        void
        domain_address_pattern(iterator begin, iterator end, bool suffix) override
        {
            this->got_domain_address_pattern = true;
            this->pattern = make_string_view(begin, end);
            this->suffix_match = suffix;
        }

        void
        regex_address_pattern(iterator begin, iterator end) override
        {
            this->got_regex_address_pattern = true;
            this->pattern = make_string_view(begin, end);
        }

        std::string_view pattern {};
        bool prefix_match = false;
        bool suffix_match = false;
        bool got_basic_address_pattern = false;
        bool got_domain_address_pattern = false;
        bool got_regex_address_pattern = false;
    };

    BOOST_AUTO_TEST_SUITE(basic_address_pattern_)

        BOOST_AUTO_TEST_CASE(ok1_)
        {
            auto const s = ""sv;
            handler h;

            address_pattern(s.begin(), s.end(), h);

            BOOST_REQUIRE(h.got_basic_address_pattern);
            BOOST_TEST(h.pattern == "");
            BOOST_TEST(!h.prefix_match);
            BOOST_TEST(!h.suffix_match);
        }

        BOOST_AUTO_TEST_CASE(ok2_)
        {
            auto const s = "pattern"sv;
            handler h;

            address_pattern(s.begin(), s.end(), h);

            BOOST_REQUIRE(h.got_basic_address_pattern);
            BOOST_TEST(h.pattern == "pattern");
            BOOST_TEST(!h.prefix_match);
            BOOST_TEST(!h.suffix_match);
        }

        BOOST_AUTO_TEST_CASE(ok3_)
        {
            auto const s = "|prefix"sv;
            handler h;

            address_pattern(s.begin(), s.end(), h);

            BOOST_REQUIRE(h.got_basic_address_pattern);
            BOOST_TEST(h.pattern == "prefix");
            BOOST_TEST(h.prefix_match);
            BOOST_TEST(!h.suffix_match);
        }

        BOOST_AUTO_TEST_CASE(ok4_)
        {
            auto const s = "suffix|"sv;
            handler h;

            address_pattern(s.begin(), s.end(), h);

            BOOST_REQUIRE(h.got_basic_address_pattern);
            BOOST_TEST(h.pattern == "suffix");
            BOOST_TEST(!h.prefix_match);
            BOOST_TEST(h.suffix_match);
        }

        BOOST_AUTO_TEST_CASE(ok5_)
        {
            auto const s = "|both|"sv;
            handler h;

            address_pattern(s.begin(), s.end(), h);

            BOOST_REQUIRE(h.got_basic_address_pattern);
            BOOST_TEST(h.pattern == "both");
            BOOST_TEST(h.prefix_match);
            BOOST_TEST(h.suffix_match);
        }

        BOOST_AUTO_TEST_CASE(err1_)
        {
            auto const s = "|"sv;
            handler h;

            BOOST_CHECK_THROW(
                address_pattern(s.begin(), s.end(), h),
                parse_error
            );
        }

    BOOST_AUTO_TEST_SUITE_END() // basic_address_pattern_

    BOOST_AUTO_TEST_SUITE(domain_address_pattern_)

        BOOST_AUTO_TEST_CASE(ok1_)
        {
            auto const s = "||domain"sv;
            handler h;

            address_pattern(s.begin(), s.end(), h);

            BOOST_REQUIRE(h.got_domain_address_pattern);
            BOOST_TEST(h.pattern == "domain");
            BOOST_TEST(!h.suffix_match);
        }

        BOOST_AUTO_TEST_CASE(ok2_)
        {
            auto const s = "||domain|"sv;
            handler h;

            address_pattern(s.begin(), s.end(), h);

            BOOST_REQUIRE(h.got_domain_address_pattern);
            BOOST_TEST(h.pattern == "domain");
            BOOST_TEST(h.suffix_match);
        }

        BOOST_AUTO_TEST_CASE(ok3_)
        {
            auto const s = "||||"sv;
            handler h;

            address_pattern(s.begin(), s.end(), h);

            BOOST_REQUIRE(h.got_domain_address_pattern);
            BOOST_TEST(h.pattern == "|");
            BOOST_TEST(h.suffix_match);
        }

        BOOST_AUTO_TEST_CASE(err1_)
        {
            auto const s = "||"sv;
            handler h;

            BOOST_CHECK_THROW(
                address_pattern(s.begin(), s.end(), h),
                parse_error
            );
        }

        BOOST_AUTO_TEST_CASE(err2_)
        {
            auto const s = "|||"sv;
            handler h;

            BOOST_CHECK_THROW(
                address_pattern(s.begin(), s.end(), h),
                parse_error
            );
        }

    BOOST_AUTO_TEST_SUITE_END() // domain_address_pattern_

    BOOST_AUTO_TEST_SUITE(regex_address_pattern_)

        BOOST_AUTO_TEST_CASE(ok1_)
        {
            auto const s = "//"sv;
            handler h;

            address_pattern(s.begin(), s.end(), h);

            BOOST_REQUIRE(h.got_regex_address_pattern);
            BOOST_TEST(h.pattern == "");
        }

        BOOST_AUTO_TEST_CASE(ok2_)
        {
            auto const s = "/regex/"sv;
            handler h;

            address_pattern(s.begin(), s.end(), h);

            BOOST_REQUIRE(h.got_regex_address_pattern);
            BOOST_TEST(h.pattern == "regex");
        }

        BOOST_AUTO_TEST_CASE(ok3_)
        {
            auto const s = "///"sv;
            handler h;

            address_pattern(s.begin(), s.end(), h);

            BOOST_REQUIRE(h.got_regex_address_pattern);
            BOOST_TEST(h.pattern == "/");
        }

        BOOST_AUTO_TEST_CASE(ok4_)
        {
            auto const s = "/foo//"sv;
            handler h;

            address_pattern(s.begin(), s.end(), h);

            BOOST_REQUIRE(h.got_regex_address_pattern);
            BOOST_TEST(h.pattern == "foo/");
        }

        BOOST_AUTO_TEST_CASE(err1_)
        {
            auto const s = "/"sv;
            handler h;

            address_pattern(s.begin(), s.end(), h);

            BOOST_REQUIRE(h.got_basic_address_pattern);
            BOOST_TEST(h.pattern == "/");
            BOOST_TEST(!h.prefix_match);
            BOOST_TEST(!h.suffix_match);
        }

        BOOST_AUTO_TEST_CASE(err2_)
        {
            auto const s = "/foo"sv;
            handler h;

            address_pattern(s.begin(), s.end(), h);

            BOOST_REQUIRE(h.got_basic_address_pattern);
            BOOST_TEST(h.pattern == "/foo");
            BOOST_TEST(!h.prefix_match);
            BOOST_TEST(!h.suffix_match);
        }

        BOOST_AUTO_TEST_CASE(err3_)
        {
            auto const s = "/foo/bar"sv;
            handler h;

            address_pattern(s.begin(), s.end(), h);

            BOOST_REQUIRE(h.got_basic_address_pattern);
            BOOST_TEST(h.pattern == "/foo/bar");
            BOOST_TEST(!h.prefix_match);
            BOOST_TEST(!h.suffix_match);
        }

    BOOST_AUTO_TEST_SUITE_END() // regex_address_pattern_

BOOST_AUTO_TEST_SUITE_END() // address_pattern_

} // namespace adblock_parser::testing
