#include <adblock_parser/json.hpp>

#include <adblock_parser.hpp>

#include "json_builder.hpp"

#include <cassert>
#include <sstream>
#include <string>
#include <string_view>

namespace adblock_parser {

to_json_result
to_json(std::string_view const s) noexcept
{
    json_builder b;

    parse_filter_list(s, b);

    return { std::move(b.rules()), std::move(b.errors()) };
}

static void
put_option_domains(std::ostream& os, json::array const& arr)
{
    bool first = true;

    for (auto const& v: arr) {
        if (!first) {
            os << '|';
        }
        else {
            first = false;
        }

        os << v.get_string().c_str();
    }
}

static void
put_filter_option(std::ostream& os, json::object const& o)
{
   if (o.contains("inverse")) {
       os << '~';
   }

   os << o.at("type").get_string().c_str();

   if (o.contains("value")) {
       os << '=';
       os << o.at("value").get_string().c_str();
   }
   else if (o.contains("domains")) {
       os << '=';
       put_option_domains(os, o.at("domains").get_array());
   }
}

static void
put_filter_options(std::ostream& os, json::array const& options)
{
    os << '$';
    bool first = true;

    for (auto const& v: options) {
        if (!first) {
            os << ',';
        }
        else {
            first = false;
        }

        put_filter_option(os, v.get_object());
    }
}

static void
put_address_pattern(std::ostream& os, json::object const& p)
{
    auto const& type = p.at("type");

    if (type == "basic") {
        if (p.contains("prefix")) {
            if (p.at("prefix").get_bool()) {
                os << '|';
            }
        }

        os << p.at("pattern").get_string().c_str();

        if (p.contains("suffix")) {
            if (p.at("suffix").get_bool()) {
                os << '|';
            }
        }
    }
    else if (type == "domain") {
        os << "||" << p.at("pattern").get_string().c_str();

        if (p.contains("suffix")) {
            if (p.at("suffix").get_bool()) {
                os << '|';
            }
        }
    }
    else {
        assert(type == "regex");
        os << '/' << p.at("pattern").get_string().c_str() << '/';
    }
}

static void
put_basic_filter_rule(std::ostream& os, json::object const& rule)
{
    put_address_pattern(os, rule.at("pattern").get_object());

    if (rule.contains("options")) {
        put_filter_options(os, rule.at("options").get_array());
    }
}

static void
put_exception_filter_rule(std::ostream& os, json::object const& rule)
{
    os << "@@";
    put_address_pattern(os, rule.at("pattern").get_object());

   if (rule.contains("options")) {
       put_filter_options(os, rule.at("options").get_array());
   }
}

static void
put_filter_domains(std::ostream& os, json::array const& arr)
{
    bool first = true;

    for (auto const& v: arr) {
        if (!first) {
            os << ',';
        }
        else {
            first = false;
        }

        os << v.get_string().c_str();
    }
}

static void
put_basic_element_hiding_rule(std::ostream& os, json::object const& rule)
{
    if (rule.contains("domains")) {
        put_filter_domains(os, rule.at("domains").get_array());
    }

    os << "##" << rule.at("selector").get_string().c_str();
}

static void
put_exception_element_hiding_rule(std::ostream& os, json::object const& rule)
{
    if (rule.contains("domains")) {
        put_filter_domains(os, rule.at("domains").get_array());
    }

    os << "#@#" << rule.at("selector").get_string().c_str();
}

static void
put_extended_element_hiding_rule(std::ostream& os, json::object const& rule)
{
    if (rule.contains("domains")) {
        put_filter_domains(os, rule.at("domains").get_array());
    }

    os << "#?#" << rule.at("selector").get_string().c_str();
}

static void
put_snippet_filter_rule(std::ostream& os, json::object const& rule)
{
    if (rule.contains("domains")) {
        put_filter_domains(os, rule.at("domains").get_array());
    }

    os << "#$#" << rule.at("snippet").get_string().c_str();
}

static void
put_comment(std::ostream& os, json::object const& rule)
{
    os << rule.at("line").get_string().c_str();
}

std::string
from_json(json::array const& rules)
{
    std::ostringstream os;

    os << "[Adblock Plus 2.0]\n";

    for (auto const& v: rules) {
       auto const& rule = v.get_object();

       auto const& type = rule.at("type");

       if (type == "basic filter rule") {
           put_basic_filter_rule(os, rule);
       }
       else if (type == "exception filter rule") {
           put_exception_filter_rule(os, rule);
       }
       else if (type == "basic element hiding rule") {
           put_basic_element_hiding_rule(os, rule);
       }
       else if (type == "exception element hiding rule") {
           put_exception_element_hiding_rule(os, rule);
       }
       else if (type == "extended element hiding rule") {
           put_extended_element_hiding_rule(os, rule);
       }
       else if (type == "snippet filter rule") {
           put_snippet_filter_rule(os, rule);
       }
       else {
           assert(type == "comment");
           put_comment(os, rule);
       }

       os << '\n';
    }

    return os.str();
}

} // namespace adblock_parser
