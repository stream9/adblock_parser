#ifndef ADBLOCK_PARSER_UTILITY_HPP
#define ADBLOCK_PARSER_UTILITY_HPP

#include <adblock_parser/char_type.hpp>

#include <cctype>
#include <string_view>

namespace adblock_parser {

inline bool
is_eol(iterator const it, iterator const end) noexcept
{
    return it == end || *it == '\n';
}

inline std::string_view
make_string_view(iterator const begin, iterator const end) noexcept
// [[expects: begin <= end]]
{
    return { begin, static_cast<size_t>(end - begin) };
}

inline iterator
skip_to_eol(iterator it, iterator const end) noexcept
// [[expects: it <= end]]
// [[ensures: it == end || *it == '\n']]
{
    while (!is_eol(it, end)) {
        ++it;
    }

    return it;
}

bool starts_with(iterator begin, iterator end, std::string_view s) noexcept;
bool istarts_with(iterator begin, iterator end, std::string_view s) noexcept;

void ltrim(iterator& begin, iterator const end) noexcept;
// [[expects: it <= end]]

void
rtrim(iterator const begin, iterator& end) noexcept;
// [[expects: it <= end]]

inline void
trim(iterator& begin, iterator& end) noexcept
// [[expects: it <= end]]
{
    if (begin == end) return;

    ltrim(begin, end);
    rtrim(begin, end);
}

} // namespace adblock_parser

#endif // ADBLOCK_PARSER_UTILITY_HPP
