#include "main.hpp"

#include <adblock_parser.hpp>
#include <adblock_parser/char_type.hpp>
#include <adblock_parser/event_handler.hpp>

#include "element_hiding_rule.hpp"
#include "error.hpp"
#include "filter_option.hpp"
#include "filter_rule.hpp"
#include "utility.hpp"

namespace adblock_parser {

/*
 * since regex might contain '$', we need different strategy to
 * detect '$' which is a separator between an address pattern and options
 */
static void
scan_maybe_regex_filter(iterator const bol, iterator const end,
                        iterator& mark, iterator& eol)
// [[expects: bol < end]]
// [[expects: *bol == '/']]
// [[ensures: is_eol(mark, end) || *mark == '$']]
// [[ensures: is_eol(eol, end)]]
// [[ensures: bol < mark && mark <= eol]]
{
    for (auto i1 = bol + 1; ; ++i1) {
        if (is_eol(i1, end)) {
            mark = i1;
            eol = i1;
            break;
        }

        if (*i1 == '$') {
            auto i2 = i1 + 1;
            if (scan_filter_options(i2, end)) {
                mark = i1;
                eol = i2;
                break;
            }
        }
    }
}

static void
scan_empty_line(iterator& bol,
                iterator const eol, event_handler& handler)
//[[expects: bol == eol]]
{
    handler.new_line(bol, eol);

    bol = eol + 1;
}

static void
scan_comment(iterator& bol,
             iterator const end, event_handler& handler)
//[[expects: bol < end]]
//[[expects: *bol == '!']]
{
    auto const eol = skip_to_eol(bol + 1, end);

    handler.new_line(bol, eol);
    handler.comment(bol, eol);

    bol = eol + 1;
}

static void
scan_exception_filter(iterator& bol,
                      iterator const end, event_handler& handler, bool& error)
{
    auto i1 = bol + 2; // skip "@@"
    auto eol = end;
    auto mark = end;

    if (!is_eol(i1, end) && *i1 == '/') {
        scan_maybe_regex_filter(i1, end, mark, eol);
    }
    else {
        for (; !is_eol(i1, end); ++i1) {
            if (*i1 == '$') {
                mark = i1;
                break;
            }
        }

        eol = skip_to_eol(i1, end);
        if (mark == end) mark = eol;
    }

    handler.new_line(bol, eol);
    if (!exception_filter_rule(bol, mark, eol, handler)) {
        error = true;
    }

    bol = eol + 1;
}

static bool
scan_element_hiding(iterator& bol, iterator const mark,
                    iterator const end, event_handler& handler, bool& error)
{
    auto i1 = mark + 1; // skip '#'

    if (*i1 == '#') {
        auto const eol = skip_to_eol(i1 + 1, end);

        handler.new_line(bol, eol);
        if (!basic_element_hiding_rule(bol, mark, eol, handler)) {
            error = true;
        }
        bol = eol + 1;
        return true;
    }
    else if (starts_with(i1, end, "@#")) {
        auto const eol = skip_to_eol(i1 + 2, end);

        handler.new_line(bol, eol);
        if (!exception_element_hiding_rule(bol, mark, eol, handler)) {
            error = true;
        }
        bol = eol + 1;
        return true;
    }
    else if (starts_with(i1, end, "?#")) {
        auto const eol = skip_to_eol(i1 + 2, end);

        handler.new_line(bol, eol);
        if (!extended_element_hiding_rule(bol, mark, eol, handler)) {
            error = true;
        }
        bol = eol + 1;
        return true;
    }

    return false;
}

static void
scan_snippet_filter(iterator& bol, iterator const mark,
                    iterator const end, event_handler& handler, bool& error)
// [[expects: bol <= mark && mark < end]]
// [[expects: starts_with(mark, end, "#$#")]]
{
    auto const eol = skip_to_eol(mark + 3, end); // skip "#?#"

    handler.new_line(bol, eol);
    if (!snippet_filter_rule(bol, mark, eol, handler)) {
        error = true;
    }

    bol = eol + 1;
}

bool
rule(iterator& bol, iterator const end, event_handler& handler)
// [[expects: bol <= end]]
{
    bool error = false;

    if (*bol == '!') {
        scan_comment(bol, end, handler);
    }
    else if (*bol == '/') { // regex or basic filter rule
        auto mark = end;
        auto eol = end;
        scan_maybe_regex_filter(bol, end, mark, eol);

        handler.new_line(bol, eol);
        if (!basic_filter_rule(bol, mark, eol, handler)) {
            error = true;
        }

        bol = eol + 1;
    }
    else if (starts_with(bol, end, "@@")) {
        scan_exception_filter(bol, end, handler, error);
    }
    else { // can't identify type from bol
        for (auto i1 = bol; ; ++i1) {
            if (is_eol(i1, end)) {
                if (bol == i1) {
                    scan_empty_line(bol, i1, handler);
                }
                else {
                    handler.new_line(bol, i1);
                    if (!basic_filter_rule(bol, i1, i1, handler)) {
                        error = true;
                    }

                    bol = i1 + 1;
                }
                break;
            }
            else if (*i1 == '$') {
                auto const mark = i1;
                auto const eol = skip_to_eol(i1 + 1, end);

                handler.new_line(bol, eol);
                if (!basic_filter_rule(bol, mark, eol, handler)) {
                    error = true;
                }
                bol = eol + 1;
                break;
            }
            else if (*i1 == '#') {
                if (scan_element_hiding(bol, i1, end, handler, error)) {
                    break;
                }
                else if (starts_with(i1, end, "#$#")) {
                    scan_snippet_filter(bol, i1, end, handler, error);
                    break;
                }
            }
        }
    }

    return !error;
}

bool
body(iterator& it, iterator const end, event_handler& handler)
// [[expects: it <= end]]
{
    auto& bol = it;
    bool error = false;

    while (bol < end) { // caution, bol could be (end + 1)
        error |= !rule(bol, end, handler);
    }

    return !error;
}

bool
header(iterator& bol, iterator const end, event_handler& handler)
// [[expects: bol <= end]]
{
    auto const eol = skip_to_eol(bol, end);
    handler.new_line(bol, eol);

    std::string_view const label = "[Adblock Plus";

    if (!starts_with(bol, eol, label)) {
        handler.error(bol, eol, "invalid header line");
        return false;
    }

    auto i1 = bol + label.size();

    auto i2 = i1;
    for (; !is_eol(i2, eol); ++i2) {
        if (*i2 == ']') break;
    }

    if (is_eol(i2, eol)) {
        handler.error(bol, eol, "invalid header line");
        return false;
    }

    trim(i1, i2);
    handler.header(bol, eol, i1, i2);

    bol = (eol == end) ? eol : eol + 1;
    return true;
}

bool
parse_filter_list(std::string_view const s, event_handler& handler)
{
    auto it = s.begin();
    auto const end = s.end();

    bool ok = header(it, end, handler);
    ok &= body(it, end, handler);

    return ok;
}

bool
parse_filter(std::string_view const s, event_handler& handler)
{
    auto it = s.begin();
    auto const end = s.end();

    return body(it, end, handler);
}

} // namespace adblock_parser
