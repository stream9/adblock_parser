#ifndef ADBLOCK_PARSER_FILTER_OPTION_HPP
#define ADBLOCK_PARSER_FILTER_OPTION_HPP

#include <adblock_parser/char_type.hpp>
#include <adblock_parser/event_handler.hpp>

namespace adblock_parser {

void basic_filter_options(iterator begin, iterator end, event_handler&);
void exception_filter_options(iterator begin, iterator end, event_handler&);

bool black_list_option(iterator begin, iterator end, event_handler&,
                       bool exception_filter = false);
bool white_list_option(iterator begin, iterator end, event_handler&);

bool scan_filter_options(iterator& it, iterator end) noexcept;

} // namespace adblock_parser

#endif // ADBLOCK_PARSER_FILTER_OPTION_HPP
