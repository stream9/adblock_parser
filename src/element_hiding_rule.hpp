#ifndef ADBLOCK_PARSER_ELEMENT_HIDING_RULE_HPP
#define ADBLOCK_PARSER_ELEMENT_HIDING_RULE_HPP

#include <adblock_parser/char_type.hpp>
#include <adblock_parser/event_handler.hpp>

namespace adblock_parser {

bool
basic_element_hiding_rule(iterator bol, iterator mark,
                          iterator eol, event_handler& handler) noexcept;
bool
exception_element_hiding_rule(iterator bol, iterator mark,
                          iterator eol, event_handler& handler) noexcept;
bool
extended_element_hiding_rule(iterator bol, iterator mark,
                          iterator eol, event_handler& handler) noexcept;
bool
snippet_filter_rule(iterator bol, iterator mark,
                    iterator eol, event_handler& handler) noexcept;

void
filter_domain(iterator begin, iterator end, event_handler& handler);

void
filter_domains(iterator begin, iterator end, event_handler& handler);

void
css_selector(iterator begin, iterator end, event_handler& handler);

} // namespace adblock_parser

#endif // ADBLOCK_PARSER_ELEMENT_HIDING_RULE_HPP
