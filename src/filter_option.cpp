#include "filter_option.hpp"

#include <adblock_parser/char_type.hpp>
#include <adblock_parser/event_handler.hpp>

#include "error.hpp"
#include "utility.hpp"

#include <string_view>

namespace adblock_parser {

static bool
boolean_option(iterator const begin, iterator const end,
               std::string_view const name)
// [[expects: begin < end]]
{
    auto i1 = begin;
    ltrim(i1, end);

    if (!istarts_with(i1, end, name)) {
        if (*i1 == '~' && istarts_with(i1 + 1, end, name)) {
            throw parse_error(begin, end, "can't inverse option");
        }
        return false;
    }

    i1 += name.size();
    ltrim(i1, end);

    if (i1 == end) {
        return true;
    }
    else if (*i1 == '=') {
        throw parse_error(i1, i1 + 1, "option can't have value");
    }

    return false;
}

static bool
inversible_option(iterator const begin, iterator const end,
                  std::string_view const name, bool& inverse)
// [[expects: begin < end]]
{
    auto i = begin;
    ltrim(i, end);

    if (*i == '~') {
        ++i;
        if (i == end || *i == '~') {
            throw parse_error(begin, end, "invalid option");
        }
        inverse = true;
    }

    return boolean_option(i, end, name);
}

static bool
value_option(iterator& it, iterator const end,
             std::string_view const name, bool const optional = false)
// [[expects: begin < end]]
{
    auto i1 = it;
    ltrim(i1, end);

    if (!istarts_with(i1, end, name)) {
        if (*i1 == '~' && starts_with(i1 + 1, end, name)) {
            throw parse_error(i1, i1 + 1, "can't inverse option");
        }
        return false;
    }

    i1 += name.size();
    ltrim(i1, end);

    if (i1 != end && *i1 == '=') {
        ++i1;
        ltrim(i1, end);
    }
    else if (!optional) {
        throw parse_error(it, end, "option must have value");
    }

    it = i1;
    return true;
}

static bool
script_option(iterator const begin, iterator const end, event_handler& handler)
// [[expects: begin < end]]
{
    bool inverse = false;

    if (inversible_option(begin, end, "script", inverse)) {
        handler.script_option(begin, end, inverse);
        return true;
    }

    return false;
}

static bool
image_option(iterator const begin, iterator const end, event_handler& handler)
// [[expects: begin < end]]
{
    bool inverse = false;

    if (inversible_option(begin, end, "image", inverse)) {
        handler.image_option(begin, end, inverse);
        return true;
    }

    return false;
}

static bool
stylesheet_option(iterator const begin, iterator const end, event_handler& handler)
// [[expects: begin < end]]
{
    bool inverse = false;

    if (inversible_option(begin, end, "stylesheet", inverse)) {
        handler.stylesheet_option(begin, end, inverse);
        return true;
    }

    return false;
}

static bool
object_option(iterator const begin, iterator const end, event_handler& handler)
// [[expects: begin < end]]
{
    bool inverse = false;

    if (inversible_option(begin, end, "object", inverse)) {
        handler.object_option(begin, end, inverse);
        return true;
    }

    return false;
}

static bool
xmlhttprequest_option(iterator const begin,
                      iterator const end, event_handler& handler)
// [[expects: begin < end]]
{
    bool inverse = false;

    if (inversible_option(begin, end, "xmlhttprequest", inverse)) {
        handler.xmlhttprequest_option(begin, end, inverse);
        return true;
    }

    return false;
}

static bool
object_subrequest_option(iterator const begin,
                         iterator const end, event_handler& handler)
// [[expects: begin < end]]
{
    bool inverse = false;

    if (inversible_option(begin, end, "object-subrequest", inverse)) {
        handler.object_subrequest_option(begin, end, inverse);
        return true;
    }

    return false;
}

static bool
subdocument_option(iterator const begin,
                   iterator const end, event_handler& handler)
// [[expects: begin < end]]
{
    bool inverse = false;

    if (inversible_option(begin, end, "subdocument", inverse)) {
        handler.subdocument_option(begin, end, inverse);
        return true;
    }

    return false;
}

static bool
ping_option(iterator const begin,
            iterator const end, event_handler& handler)
// [[expects: begin < end]]
{
    bool inverse = false;

    if (inversible_option(begin, end, "ping", inverse)) {
        handler.ping_option(begin, end, inverse);
        return true;
    }

    return false;
}

static bool
websocket_option(iterator const begin,
                 iterator const end, event_handler& handler)
// [[expects: begin < end]]
{
    bool inverse = false;

    if (inversible_option(begin, end, "websocket", inverse)) {
        handler.websocket_option(begin, end, inverse);
        return true;
    }

    return false;
}

static bool
webrtc_option(iterator const begin,
              iterator const end, event_handler& handler)
// [[expects: begin < end]]
{
    bool inverse = false;

    if (inversible_option(begin, end, "webrtc", inverse)) {
        handler.webrtc_option(begin, end, inverse);
        return true;
    }

    return false;
}

static bool
popup_option(iterator const begin,
              iterator const end, event_handler& handler)
// [[expects: begin < end]]
{
    if (boolean_option(begin, end, "popup")) {
        handler.popup_option(begin, end);
        return true;
    }

    return false;
}

static bool
media_option(iterator const begin,
             iterator const end, event_handler& handler)
// [[expects: begin < end]]
{
    bool inverse = false;

    if (inversible_option(begin, end, "media", inverse)) {
        handler.media_option(begin, end, inverse);
        return true;
    }

    return false;
}

static bool
font_option(iterator const begin,
            iterator const end, event_handler& handler)
// [[expects: begin < end]]
{
    bool inverse = false;

    if (inversible_option(begin, end, "font", inverse)) {
        handler.font_option(begin, end, inverse);
        return true;
    }

    return false;
}

static bool
other_option(iterator const begin,
             iterator const end, event_handler& handler)
// [[expects: begin < end]]
{
    bool inverse = false;

    if (inversible_option(begin, end, "other", inverse)) {
        handler.other_option(begin, end, inverse);
        return true;
    }

    return false;
}

static bool
third_party_option(iterator const begin,
                   iterator const end, event_handler& handler)
// [[expects: begin < end]]
{
    bool inverse = false;

    if (inversible_option(begin, end, "third-party", inverse)) {
        handler.third_party_option(begin, end, inverse);
        return true;
    }

    return false;
}

static void
filter_domain(iterator const begin,
              iterator const end, event_handler& handler)
{
    if (begin == end) {
        throw parse_error(begin, end, "empty domain");
    }
    handler.filter_domain(begin, end);
}

static bool
domain_option(iterator const begin,
              iterator const end, event_handler& handler)
// [[expects: begin < end]]
{
    auto i1 = begin;

    if (!value_option(i1, end, "domain")) {
        return false;
    }

    handler.begin_domain_option(begin, end);

    for (auto i2 = i1; i2 != end; ++i2) {
        if (*i2 == '|') {
            filter_domain(i1, i2, handler);
            i1 = i2 + 1;
        }
    }
    filter_domain(i1, end, handler);

    handler.end_domain_option(begin, end);
    return true;
}

static bool
site_key_option(iterator const begin,
                iterator const end, event_handler& handler)
// [[expects: begin < end]]
{
    auto i1 = begin;

    if (!value_option(i1, end, "sitekey")) {
        return false;
    }

    handler.site_key_option(begin, end, i1, end);
    return true;
}

static bool
csp_option(iterator const begin, iterator const end,
           event_handler& handler, bool const optional)
// [[expects: begin < end]]
{
    auto i1 = begin;

    if (!value_option(i1, end, "csp", optional)) {
        return false;
    }

    handler.csp_option(begin, end, i1, end);
    return true;
}

static bool
rewrite_option(iterator const begin,
               iterator const end, event_handler& handler)
// [[expects: begin < end]]
{
    auto i1 = begin;

    if (!value_option(i1, end, "rewrite")) {
        return false;
    }

    handler.rewrite_option(begin, end, i1, end);
    return true;
}

static bool
match_case_option(iterator const begin,
                  iterator const end, event_handler& handler)
// [[expects: begin < end]]
{
    if (boolean_option(begin, end, "match-case")) {
        handler.match_case_option(begin, end);
        return true;
    }

    return false;
}

static bool
collapse_option(iterator const begin,
                iterator const end, event_handler& handler)
// [[expects: begin < end]]
{
    bool inverse = false;

    if (inversible_option(begin, end, "collapse", inverse)) {
        handler.collapse_option(begin, end, inverse);
        return true;
    }

    return false;
}

static bool
do_not_track_option(iterator const begin,
                  iterator const end, event_handler& handler)
// [[expects: begin < end]]
{
    if (boolean_option(begin, end, "donottrack")) {
        handler.do_not_track_option(begin, end);
        return true;
    }

    return false;
}

static bool
document_option(iterator const begin,
                   iterator const end, event_handler& handler)
// [[expects: begin < end]]
{
    bool inverse = false;

    if (inversible_option(begin, end, "document", inverse)) {
        handler.document_option(begin, end, inverse);
        return true;
    }

    return false;
}

static bool
elem_hide_option(iterator const begin,
                   iterator const end, event_handler& handler)
// [[expects: begin < end]]
{
    bool inverse = false;

    if (inversible_option(begin, end, "elemhide", inverse)) {
        handler.elem_hide_option(begin, end, inverse);
        return true;
    }

    return false;
}

static bool
generic_hide_option(iterator const begin,
              iterator const end, event_handler& handler)
// [[expects: begin < end]]
{
    if (boolean_option(begin, end, "generichide")) {
        handler.generic_hide_option(begin, end);
        return true;
    }

    return false;
}

static bool
generic_block_option(iterator const begin,
              iterator const end, event_handler& handler)
// [[expects: begin < end]]
{
    if (boolean_option(begin, end, "genericblock")) {
        handler.generic_block_option(begin, end);
        return true;
    }

    return false;
}

bool
black_list_option(iterator const begin, iterator const end,
                  event_handler& handler, bool const exception_filter/*= false*/)
{
    if (begin == end) {
        throw parse_error(begin, end, "empty option");
    }

    auto i1 = begin;
    ltrim(i1, end);

    if (i1 == end) return false;

    auto front = *i1;

    if (front == '~') {
        auto const i2 = i1 + 1;
        if (i2 == end) {
            return false;
        }
        else {
            front = *i2;
        }
    }

    switch (std::tolower(front)) {
        case 'c':
            return collapse_option(begin, end, handler)
                || csp_option(begin, end, handler, exception_filter);
        case 'd':
            return do_not_track_option(begin, end, handler)
                || domain_option(begin, end, handler);
        case 'f':
            return font_option(begin, end, handler);
        case 'i':
            return image_option(begin, end, handler);
        case 'm':
            return match_case_option(begin, end, handler)
                || media_option(begin, end, handler);
        case 'o':
            return object_option(begin, end, handler)
                || object_subrequest_option(begin, end, handler)
                || other_option(begin, end, handler);
        case 'p':
            return ping_option(begin, end, handler)
                || popup_option(begin, end, handler);
        case 'r':
            return rewrite_option(begin, end, handler);
        case 's':
            return script_option(begin, end, handler)
                || site_key_option(begin, end, handler)
                || stylesheet_option(begin, end, handler)
                || subdocument_option(begin, end, handler);
        case 't':
            return third_party_option(begin, end, handler);
        case 'w':
            return webrtc_option(begin, end, handler)
                || websocket_option(begin, end, handler);
        case 'x':
            return xmlhttprequest_option(begin, end, handler);
        default:
            return false;
    }
}

bool
white_list_option(iterator const begin,
                  iterator const end, event_handler& handler)
{
    return document_option(begin, end, handler)
        || elem_hide_option(begin, end, handler)
        || generic_hide_option(begin, end, handler)
        || generic_block_option(begin, end, handler)
        ;
}

template<typename Option>
void
filter_options(iterator const begin, iterator const end, Option option,
               event_handler& handler)
// [[expects: begin < end]]
// [[expects: *begin == '$']]
{
    handler.begin_filter_options(begin, end);

    auto i1 = begin + 1; // skip '$'
    for (auto i2 = i1; i2 != end; ++i2) {
        if (*i2 == ',') {
            option(i1, i2, handler);

            i1 = i2 + 1;
        }
    }

    option(i1, end, handler);

    handler.end_filter_options(begin, end);
}

void
basic_filter_options(iterator const begin,
                     iterator const end, event_handler& handler)
// [[expects: begin < end]]
// [[expects: *begin == '$']]
{
    auto option = [](auto begin, auto end, auto& handler) {
        if (!black_list_option(begin, end, handler)) {
            if (white_list_option(begin, end, handler)) {
                throw parse_error(begin, end,
                    "only exception filter rule can have white list option");
            }
            else {
                throw parse_error(begin, end, "invalid option");
            }
        }
    };

    filter_options(begin, end, option, handler);
}

void
exception_filter_options(iterator const begin,
                         iterator const end, event_handler& handler)
// [[expects: begin < end]]
// [[expects: *begin == '$']]
{
    auto option = [](auto begin, auto end, auto& handler) {
        if (   !black_list_option(begin, end, handler, true)
            && !white_list_option(begin, end, handler))
        {
            throw parse_error(begin, end, "invalid option");
        }
    };

    filter_options(begin, end, option, handler);
}

static void
scan_name(iterator& it, iterator const end) noexcept
// [[expects: it <= end]]
{
    auto is_name_char = [](auto c) {
        return c == '-'
            || ('a' <= c && c <= 'z')
            || ('A' <= c && c <= 'Z');
    };

    while (!is_eol(it, end) && is_name_char(*it)) {
        ++it;
    }
}

static void
scan_value(iterator& it, iterator const end) noexcept
// [[expects: it <= end]]
// [[ensures: is_eol(it) || *it == ',']]
{
    ltrim(it, end);
    while (!is_eol(it, end) && *it != ',') {
        ++it;
    }
}

static bool
scan_option(iterator& it, iterator const end) noexcept
// [[expects: it <= end]]
// [[ensures: is_eol(it) || *it == ',']]
{
    ltrim(it, end);

    if (is_eol(it, end)) return false;

    if (*it == '~') {
        ++it;
    }

    scan_name(it, end);

    if (*it == '=') {
        scan_value(++it, end);
    }

    return true;
}

bool
scan_filter_options(iterator& it, iterator const end) noexcept
// [[expects: begin <= end]]
{
    auto i1 = it;

    if (!scan_option(i1, end)) return false;

    ltrim(i1, end);
    if (is_eol(i1, end)) {
        it = i1;
        return true;
    }

    while (*i1 == ',') {
        if (!scan_option(++i1, end)) return false;

        ltrim(i1, end);
        if (is_eol(i1, end)) {
            it = i1;
            return true;
        }
    }

    return false;
}

} // namespace adblock_parser
