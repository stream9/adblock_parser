#include "utility.hpp"

#include <adblock_parser/char_type.hpp>

#include <cctype>
#include <string_view>

namespace adblock_parser {

bool
starts_with(iterator const begin, iterator const end,
            std::string_view const s) noexcept
{
    auto i1 = begin;
    auto i2 = s.begin();
    auto const e2 = s.end();

    for (; !is_eol(i1, end) && i2 != e2; ++i1, ++i2) {
        if (*i1 != *i2) break;
    }

    return i2 == e2;
}

bool
istarts_with(iterator const begin, iterator const end,
             std::string_view const s) noexcept
{
    auto i1 = begin;
    auto i2 = s.begin();
    auto const e2 = s.end();

    for (; !is_eol(i1, end) && i2 != e2; ++i1, ++i2) {
        if (std::toupper(*i1) != std::toupper(*i2)) break;
    }

    return i2 == e2;
}

void
ltrim(iterator& it, iterator const end) noexcept
// [[expects: it <= end]]
{
    if (it == end) return;

    for (; it != end; ++it) {
        if (*it == '\n' || !std::isspace(*it)) break;
    }
}

void
rtrim(iterator const begin, iterator& end) noexcept
// [[expects: it <= end]]
{
    if (begin == end) return;

    for (; begin != end; --end) {
        auto const c = *(end - 1);

        if (c == '\n' || !std::isspace(c)) break;
    }
}

} // namespace adblock_parser
