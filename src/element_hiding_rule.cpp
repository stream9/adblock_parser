#include "element_hiding_rule.hpp"

#include <adblock_parser/char_type.hpp>
#include <adblock_parser/event_handler.hpp>

#include "error.hpp"
#include "utility.hpp"

namespace adblock_parser {

void
filter_domain(iterator begin,
              iterator end, event_handler& handler)
// [[expects: begin <= end]]
{
    if (begin == end) {
        throw parse_error(begin, end, "empty domain");
    }

    trim(begin, end);
    handler.filter_domain(begin, end);
}

void
filter_domains(iterator const begin,
               iterator const end, event_handler& handler)
// [[expects: begin <= end]]
{
    if (begin == end) return;

    handler.begin_filter_domains(begin, end);

    auto i1 = begin;
    for (auto i2 = i1; i2 != end; ++i2) {
        if (*i2 == ',') {
            filter_domain(i1, i2, handler);

            i1 = i2 + 1;
        }
    }

    filter_domain(i1, end, handler);

    handler.end_filter_domains(begin, end);
}

void
css_selector(iterator const begin,
             iterator const end, event_handler& handler)
// [[expects: begin <= end]]
{
    if (begin != end) {
        handler.css_selector(begin, end);
    }
    else {
        throw parse_error(begin, end, "empty CSS selector");
    }
}

bool
basic_element_hiding_rule(iterator const bol, iterator const mark,
                          iterator const eol, event_handler& handler) noexcept
// [[expects: bol <= mark && mark < eol]]
// [[expects: starts_with(mark, eol, "##")]]
{
    try {
        handler.begin_basic_element_hiding_rule(bol, eol);

        filter_domains(bol, mark, handler);

        auto i1 = mark + 2; // skip "##"

        css_selector(i1, eol, handler);

        handler.end_basic_element_hiding_rule(bol, eol);
        return true;
    }
    catch (parse_error const& e) {
        handler.error(e.begin(), e.end(), e.what());
        return false;
    }
}

bool
exception_element_hiding_rule(iterator const bol, iterator const mark,
                              iterator const eol, event_handler& handler) noexcept
// [[expects: bol <= mark && mark < eol]]
// [[expects: starts_with(mark, eol, "#@#")]]
{
    try {
        handler.begin_exception_element_hiding_rule(bol, eol);

        filter_domains(bol, mark, handler);

        auto i1 = mark + 3; // skip "#@#"

        css_selector(i1, eol, handler);

        handler.end_exception_element_hiding_rule(bol, eol);
        return true;
    }
    catch (parse_error const& e) {
        handler.error(e.begin(), e.end(), e.what());
        return false;
    }
}

bool
extended_element_hiding_rule(iterator const bol, iterator const mark,
                             iterator const eol, event_handler& handler) noexcept
// [[expects: bol <= mark && mark < eol]]
// [[expects: starts_with(mark, eol, "#?#")]]
{
    try {
        handler.begin_extended_element_hiding_rule(bol, eol);

        filter_domains(bol, mark, handler);

        auto i1 = mark + 3; // skip "#?#"

        css_selector(i1, eol, handler);

        handler.end_extended_element_hiding_rule(bol, eol);
        return true;
    }
    catch (parse_error const& e) {
        handler.error(e.begin(), e.end(), e.what());
        return false;
    }
}

void
snippet(iterator const begin,
        iterator const end, event_handler& handler)
// [[expects: begin <= end]]
{
    if (begin != end) {
        handler.snippet(begin, end);
    }
    else {
        throw parse_error(begin, end, "empty snippet");
    }
}

bool
snippet_filter_rule(iterator const bol, iterator const mark,
                    iterator const eol, event_handler& handler) noexcept
// [[expects: bol <= mark && mark < eol]]
// [[expects: starts_with(mark, eol, "#$#")]]
{
    try {
        handler.begin_snippet_filter_rule(bol, eol);

        filter_domains(bol, mark, handler);

        auto i1 = mark + 3; // skip "#$#"

        snippet(i1, eol, handler);

        handler.end_snippet_filter_rule(bol, eol);
        return true;
    }
    catch (parse_error const& e) {
        handler.error(e.begin(), e.end(), e.what());
        return false;
    }
}

} // namespace adblock_parser
