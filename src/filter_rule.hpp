#ifndef ADBLOCK_PARSER_FILTER_RULE_HPP
#define ADBLOCK_PARSER_FILTER_RULE_HPP

#include <adblock_parser/char_type.hpp>
#include <adblock_parser/event_handler.hpp>

namespace adblock_parser {

bool
basic_filter_rule(iterator bol, iterator mark,
                  iterator eol, event_handler& handler) noexcept;
bool
exception_filter_rule(iterator bol, iterator mark,
                      iterator eol, event_handler& handler) noexcept;

} // namespace adblock_parser

#endif // ADBLOCK_PARSER_FILTER_RULE_HPP
