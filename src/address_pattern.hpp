#ifndef ADBLOCK_PARSER_ADRESS_PATTERN_HPP
#define ADBLOCK_PARSER_ADRESS_PATTERN_HPP

#include <adblock_parser/char_type.hpp>
#include <adblock_parser/event_handler.hpp>

namespace adblock_parser {

void address_pattern(iterator begin, iterator end, event_handler& handler);

} // namespace adblock_parser

#endif // ADBLOCK_PARSER_ADRESS_PATTERN_HPP
