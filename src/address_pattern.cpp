#include "address_pattern.hpp"

#include <adblock_parser/char_type.hpp>
#include <adblock_parser/event_handler.hpp>

#include "error.hpp"

#include <cassert>

namespace adblock_parser {

void
address_pattern(iterator const begin, iterator const end, event_handler& handler)
// [[expects: begin <= end]]
{
    auto const len = end - begin;

    if (len == 0) {
        handler.basic_address_pattern(begin, end, false, false);
    }
    else if (len >= 2 && *begin == '/' && *(end - 1) == '/') {
        handler.regex_address_pattern(begin + 1, end - 1);
    }
    else {
        auto i1 = begin;
        auto i2 = end;

        auto prefix = false;
        auto domain = false;
        if (*i1 == '|') {
            ++i1;
            if (i1 == i2) {
                throw parse_error(begin, end, "constrained empty pattern");
            }
            else if (*i1 == '|') {
                ++i1;
                if (i1 == i2) {
                    throw parse_error(begin, end, "constrained empty pattern");
                }
                domain = true;
            }
            else {
                prefix = true;
            }
        }
        assert(!(prefix && domain));
        assert(i1 != i2);

        auto suffix = false;
        if (*(i2 - 1) == '|') {
            suffix = true;
            --i2;
            if (i1 == i2) {
                throw parse_error(begin, end, "constrained empty pattern");
            }
        }

        assert(i1 != i2);

        if (domain) {
            handler.domain_address_pattern(i1, i2, suffix);
        }
        else {
            handler.basic_address_pattern(i1, i2, prefix, suffix);
        }
    }
}

} // namespace adblock_parser
