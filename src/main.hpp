#ifndef ADBLOCK_PARSER_MAIN_HPP
#define ADBLOCK_PARSER_MAIN_HPP

#include <adblock_parser/char_type.hpp>
#include <adblock_parser/event_handler.hpp>

namespace adblock_parser {

bool header(iterator& begin, iterator end, event_handler&);
bool body(iterator& begin, iterator end, event_handler&);
bool rule(iterator& begin, iterator end, event_handler&);

} // namespace adblock_parser

#endif // ADBLOCK_PARSER_MAIN_HPP
