#include "json_builder.hpp"

#include <adblock_parser/char_type.hpp>
#include <adblock_parser/event_handler.hpp>
#include <adblock_parser/json.hpp>

#include "utility.hpp"

namespace adblock_parser {

//
// filter rule
//
void json_builder::
begin_basic_filter_rule(iterator /*bol*/, iterator /*eol*/)
{
    m_pattern = {};
    m_options = {};
}

void json_builder::
end_basic_filter_rule(iterator /*bol*/, iterator /*eol*/)
{
    json::object rule;

    rule["type"] = "basic filter rule";
    rule["pattern"] = std::move(m_pattern);

    if (!m_options.empty()) {
        rule["options"] = std::move(m_options);
    }

    m_rules.push_back(std::move(rule));
}

void json_builder::
begin_exception_filter_rule(iterator /*bol*/, iterator /*eol*/)
{
    m_pattern = {};
    m_options = {};
}

void json_builder::
end_exception_filter_rule(iterator /*bol*/, iterator /*eol*/)
{
    json::object rule;

    rule["type"] = "exception filter rule";
    rule["pattern"] = std::move(m_pattern);

    if (!m_options.empty()) {
        rule["options"] = std::move(m_options);
    }

    m_rules.push_back(std::move(rule));
}

// address pattern
void json_builder::
basic_address_pattern(iterator begin, iterator end, bool prefix, bool suffix)
{
    m_pattern["type"] = "basic";
    m_pattern["pattern"] = make_string_view(begin, end);
    if (prefix) {
        m_pattern["prefix"] = prefix;
    }
    if (suffix) {
        m_pattern["suffix"] = suffix;
    }
}

void json_builder::
domain_address_pattern(iterator begin, iterator end, bool suffix)
{
    m_pattern["type"] = "domain";
    m_pattern["pattern"] = make_string_view(begin, end);
    if (suffix) {
        m_pattern["suffix"] = suffix;
    }
}

void json_builder::
regex_address_pattern(iterator begin, iterator end)
{
    m_pattern["type"] = "regex";
    m_pattern["pattern"] = make_string_view(begin, end);
}

// filter option
void json_builder::
begin_filter_options(iterator /*begin*/, iterator /*end*/)
{}

void json_builder::
end_filter_options(iterator /*begin*/, iterator /*end*/)
{}

void json_builder::
script_option(iterator/*begin*/, iterator/*end*/, bool inverse)
{
    json::object option;
    option["type"] = "script";
    if (inverse) option["inverse"] = inverse;

    m_options.push_back(std::move(option));
}

void json_builder::
image_option(iterator/*begin*/, iterator/*end*/, bool inverse)
{
    json::object option;
    option["type"] = "image";
    if (inverse) option["inverse"] = inverse;

    m_options.push_back(std::move(option));
}

void json_builder::
stylesheet_option(iterator/*begin*/, iterator/*end*/, bool inverse)
{
    json::object option;
    option["type"] = "stylesheet";
    if (inverse) option["inverse"] = inverse;

    m_options.push_back(std::move(option));
}

void json_builder::
object_option(iterator/*begin*/, iterator/*end*/, bool inverse)
{
    json::object option;
    option["type"] = "object";
    if (inverse) option["inverse"] = inverse;

    m_options.push_back(std::move(option));
}

void json_builder::
xmlhttprequest_option(iterator/*begin*/, iterator/*end*/, bool inverse)
{
    json::object option;
    option["type"] = "xmlhttprequest";
    if (inverse) option["inverse"] = inverse;

    m_options.push_back(std::move(option));
}

void json_builder::
object_subrequest_option(iterator/*begin*/, iterator/*end*/, bool inverse)
{
    json::object option;
    option["type"] = "object-subrequest";
    if (inverse) option["inverse"] = inverse;

    m_options.push_back(std::move(option));
}
void json_builder::
subdocument_option(iterator/*begin*/, iterator/*end*/, bool inverse)
{
    json::object option;
    option["type"] = "subdocument";
    if (inverse) option["inverse"] = inverse;

    m_options.push_back(std::move(option));
}

void json_builder::
ping_option(iterator/*begin*/, iterator/*end*/, bool inverse)
{
    json::object option;
    option["type"] = "ping";
    if (inverse) option["inverse"] = inverse;

    m_options.push_back(std::move(option));
}

void json_builder::
websocket_option(iterator/*begin*/, iterator/*end*/, bool inverse)
{
    json::object option;
    option["type"] = "websocket";
    if (inverse) option["inverse"] = inverse;

    m_options.push_back(std::move(option));
}

void json_builder::
webrtc_option(iterator/*begin*/, iterator/*end*/, bool inverse)
{
    json::object option;
    option["type"] = "webrtc";
    if (inverse) option["inverse"] = inverse;

    m_options.push_back(std::move(option));
}

void json_builder::
popup_option(iterator/*begin*/, iterator/*end*/)
{
    json::object option;
    option["type"] = "popup";

    m_options.push_back(std::move(option));
}

void json_builder::
media_option(iterator/*begin*/, iterator/*end*/, bool inverse)
{
    json::object option;
    option["type"] = "media";
    if (inverse) option["inverse"] = inverse;

    m_options.push_back(std::move(option));
}

void json_builder::
font_option(iterator/*begin*/, iterator/*end*/, bool inverse)
{
    json::object option;
    option["type"] = "font";
    if (inverse) option["inverse"] = inverse;

    m_options.push_back(std::move(option));
}

void json_builder::
other_option(iterator/*begin*/, iterator/*end*/, bool inverse)
{
    json::object option;
    option["type"] = "other";
    if (inverse) option["inverse"] = inverse;

    m_options.push_back(std::move(option));
}

void json_builder::
third_party_option(iterator/*begin*/, iterator/*end*/, bool inverse)
{
    json::object option;
    option["type"] = "third-party";
    if (inverse) option["inverse"] = inverse;

    m_options.push_back(std::move(option));
}

void json_builder::
match_case_option(iterator/*begin*/, iterator/*end*/)
{
    json::object option;
    option["type"] = "match-case";

    m_options.push_back(std::move(option));
}

void json_builder::
collapse_option(iterator/*begin*/, iterator/*end*/, bool inverse)
{
    json::object option;
    option["type"] = "collapse";
    if (inverse) option["inverse"] = inverse;

    m_options.push_back(std::move(option));
}

void json_builder::
do_not_track_option(iterator/*begin*/, iterator/*end*/)
{
    json::object option;
    option["type"] = "donottrack";

    m_options.push_back(std::move(option));
}

void json_builder::
begin_domain_option(iterator /*begin*/, iterator /*end*/)
{
    m_domains = {};
}

void json_builder::
filter_domain(iterator begin, iterator end)
{
    m_domains.push_back(make_string_view(begin, end));
}

void json_builder::
end_domain_option(iterator /*begin*/, iterator /*end*/)
{
    json::object option;
    option["type"] = "domain";
    option["domains"] = std::move(m_domains);

    m_options.push_back(std::move(option));
}

void json_builder::
site_key_option(iterator/*begin*/, iterator/*end*/,
                iterator value_begin, iterator value_end)
{
    json::object option;
    option["type"] = "sitekey";
    option["value"] = make_string_view(value_begin, value_end);

    m_options.push_back(std::move(option));
}

void json_builder::
csp_option(iterator/*begin*/, iterator/*end*/,
           iterator value_begin, iterator value_end)
{
    json::object option;
    option["type"] = "csp";
    option["value"] = make_string_view(value_begin, value_end);

    m_options.push_back(std::move(option));
}

void json_builder::
rewrite_option(iterator/*begin*/, iterator/*end*/,
               iterator value_begin, iterator value_end)
{
    json::object option;
    option["type"] = "rewrite";
    option["value"] = make_string_view(value_begin, value_end);

    m_options.push_back(std::move(option));
}

//
// element hide rule
//

// basic element hiding rule
void json_builder::
begin_basic_element_hiding_rule(iterator /*begin*/, iterator /*end*/)
{
    m_domains = {};
    m_body = {};
}

void json_builder::
end_basic_element_hiding_rule(iterator /*begin*/, iterator /*end*/)
{
    json::object rule;

    rule["type"] = "basic element hiding rule";
    rule["selector"] = std::move(m_body);
    if (!m_domains.empty()) rule["domains"] = std::move(m_domains);

    m_rules.push_back(std::move(rule));
}

// exception element hiding rule
void json_builder::
begin_exception_element_hiding_rule(iterator /*begin*/, iterator /*end*/)
{
    m_domains = {};
    m_body = {};
}

void json_builder::
end_exception_element_hiding_rule(iterator /*begin*/, iterator /*end*/)
{
    json::object rule;

    rule["type"] = "exception element hiding rule";
    rule["selector"] = std::move(m_body);
    if (!m_domains.empty()) rule["domains"] = std::move(m_domains);

    m_rules.push_back(std::move(rule));
}

// extended element hiding rule
void json_builder::
begin_extended_element_hiding_rule(iterator /*begin*/, iterator /*end*/)
{
    m_domains = {};
    m_body = {};
}

void json_builder::
end_extended_element_hiding_rule(iterator /*begin*/, iterator /*end*/)
{
    json::object rule;

    rule["type"] = "extended element hiding rule";
    rule["selector"] = std::move(m_body);
    if (!m_domains.empty()) rule["domains"] = std::move(m_domains);

    m_rules.push_back(std::move(rule));
}

// element hiding domain(s)
void json_builder::
begin_filter_domains(iterator /*begin*/, iterator /*end*/)
{}

void json_builder::
end_filter_domains(iterator /*begin*/, iterator /*end*/)
{}

void json_builder::
css_selector(iterator begin, iterator end)
{
    m_body = make_string_view(begin, end);
}

//
// snippet filter rule
//
void json_builder::
begin_snippet_filter_rule(iterator /*begin*/, iterator /*end*/)
{
    m_domains = {};
    m_body = {};
}

void json_builder::
end_snippet_filter_rule(iterator /*begin*/, iterator /*end*/)
{
    json::object rule;

    rule["type"] = "snippet filter rule";
    rule["snippet"] = std::move(m_body);
    if (!m_domains.empty()) rule["domains"] = std::move(m_domains);

    m_rules.push_back(std::move(rule));
}

void json_builder::
snippet(iterator begin, iterator end)
{
    m_body = make_string_view(begin, end);
}

//
// miscellaneous
//
void json_builder::
new_line(iterator bol, iterator eol)
{
    ++m_line_no;
    m_line = make_string_view(bol, eol);
}

void json_builder::
comment(iterator bol, iterator eol)
{
    json::object rule;

    rule["type"] = "comment";
    rule["line"] = make_string_view(bol, eol);

    m_rules.push_back(std::move(rule));
}

void json_builder::
error(iterator begin, iterator /*end*/, std::string_view msg)
{
    json::object error;

    error["line_no"] = m_line_no;
    error["column"] = begin - m_line.begin();
    error["line"] = m_line;
    error["message"] = msg;

    m_errors.push_back(std::move(error));
}

} // namespace adblock_parser
