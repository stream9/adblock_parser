#ifndef ADBLOCK_PARSER_ERROR_HPP
#define ADBLOCK_PARSER_ERROR_HPP

#include <adblock_parser/char_type.hpp>

#include <stdexcept>
#include <string>

namespace adblock_parser {

class parse_error : public std::runtime_error
{
public:
    parse_error(iterator const begin,
                iterator const end, std::string const& s)
        : std::runtime_error { s }
        , m_begin { begin }
        , m_end { end }
    {}

    iterator begin() const { return m_begin; }
    iterator end() const { return m_end; }

private:
    iterator m_begin;
    iterator m_end;
};

} // namespace adblock_parser

#endif // ADBLOCK_PARSER_ERROR_HPP
