#include "filter_rule.hpp"

#include <adblock_parser/char_type.hpp>
#include <adblock_parser/event_handler.hpp>

#include "address_pattern.hpp"
#include "utility.hpp"
#include "error.hpp"
#include "filter_option.hpp"

namespace adblock_parser {

bool
basic_filter_rule(iterator const bol, iterator const mark,
                  iterator const eol, event_handler& handler) noexcept
// [[expects: bol < eol]]
// [[expects: bol <= mark && mark <= eol]]
// [[expects: (bol != mark) || (mark != eol)]]
{
    try {
        handler.begin_basic_filter_rule(bol, eol);

        address_pattern(bol, mark, handler);

        if (mark < eol) {
            basic_filter_options(mark, eol, handler);
        }

        handler.end_basic_filter_rule(bol, eol);
        return true;
    }
    catch (parse_error const& e) {
        handler.error(e.begin(), e.end(), e.what());
        return false;
    }
}

bool
exception_filter_rule(iterator const bol, iterator const mark,
                      iterator const eol, event_handler& handler) noexcept
// [[expects: bol < mark && mark <= eol]]
// [[expects: starts_with(bol. eol, "@@")]]
{
    try {
        auto i1 = bol + 2; // skip "@@";
        if (i1 == eol) {
            throw parse_error(i1, eol, "empty exception filter rule");
        }

        handler.begin_exception_filter_rule(bol, eol);

        address_pattern(i1, mark, handler);

        if (mark < eol) {
            exception_filter_options(mark, eol, handler);
        }

        handler.end_exception_filter_rule(bol, eol);
        return true;
    }
    catch (parse_error const& e) {
        handler.error(e.begin(), e.end(), e.what());
        return false;
    }
}

} // namespace adblock_parser
