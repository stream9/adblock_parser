#include "validator.hpp"

#include <adblock_parser.hpp>
#include <adblock_parser/char_type.hpp>

#include "utility.hpp"

#include <cctype>
#include <fstream>
#include <iostream>
#include <regex>
#include <sstream>
#include <string_view>

#include <boost/format.hpp>

namespace adblock_parser {

static bool
is_letter(char_type const c)
{
    return std::isalpha(c) || (!std::isprint(c) && !std::iscntrl(c));
}

//
// filter rule
//
void validator::
begin_basic_filter_rule(iterator /*bol*/, iterator /*eol*/) {}

void validator::
end_basic_filter_rule(iterator /*bol*/, iterator /*eol*/) {}

void validator::
begin_exception_filter_rule(iterator /*bol*/, iterator /*eol*/) {}

void validator::
end_exception_filter_rule(iterator /*bol*/, iterator /*eol*/) {}

// address pattern
void validator::
basic_address_pattern(iterator const begin, iterator const end,
                      bool const prefix, bool const suffix)
{
    if ((prefix || suffix) && begin == end) {
        error(begin, end, "empty address pattern");
    }
    else {
        validate_address_pattern(begin, end);
    }
}

void validator::
domain_address_pattern(iterator const begin, iterator const end,
                       bool/*suffix*/)
{
    if (begin == end) {
        error(begin, end, "empty address pattern");
    }
    else {
        validate_domain_pattern(begin, end);
    }
}

void validator::
regex_address_pattern(iterator const begin, iterator const end)
{
    validate_regex_pattern(begin, end);
}

// filter option
void validator::
begin_filter_options(iterator /*begin*/, iterator /*end*/)
{
    m_options = {};
}

void validator::
end_filter_options(iterator /*begin*/, iterator /*end*/) {}

void validator::
script_option(iterator const begin, iterator const end, bool /*inverse*/)
{
    add_option(begin, end, "script");
}

void validator::
image_option(iterator const begin, iterator const end, bool /*inverse*/)
{
    add_option(begin, end, "image");
}

void validator::
stylesheet_option(iterator const begin, iterator const end, bool /*inverse*/)
{
    add_option(begin, end, "stylesheet");
}

void validator::
object_option(iterator const begin, iterator const end, bool /*inverse*/)
{
    add_option(begin, end, "object");
}

void validator::
xmlhttprequest_option(iterator const begin, iterator const end, bool /*inverse*/)
{
    add_option(begin, end, "xmlhttprequest");
}

void validator::
object_subrequest_option(iterator const begin, iterator const end, bool /*inverse*/)
{
    add_option(begin, end, "object_subrequest");
}

void validator::
subdocument_option(iterator const begin, iterator const end, bool /*inverse*/)
{
    add_option(begin, end, "subdocument");
}

void validator::
ping_option(iterator const begin, iterator const end, bool /*inverse*/)
{
    add_option(begin, end, "ping");
}

void validator::
websocket_option(iterator const begin, iterator const end, bool /*inverse*/)
{
    add_option(begin, end, "websocket");
}

void validator::
webrtc_option(iterator const begin, iterator const end, bool /*inverse*/)
{
    add_option(begin, end, "webrtc");
}

void validator::
popup_option(iterator const begin, iterator const end)
{
    add_option(begin, end, "popup");
}

void validator::
media_option(iterator const begin, iterator const end, bool /*inverse*/)
{
    add_option(begin, end, "media");
}

void validator::
font_option(iterator const begin, iterator const end, bool /*inverse*/)
{
    add_option(begin, end, "font");
}

void validator::
other_option(iterator const begin, iterator const end, bool /*inverse*/)
{
    add_option(begin, end, "other");
}

void validator::
third_party_option(iterator const begin, iterator const end, bool /*inverse*/)
{
    add_option(begin, end, "third_party");
}

void validator::
match_case_option(iterator const begin, iterator const end)
{
    add_option(begin, end, "match_case");
}

void validator::
collapse_option(iterator const begin, iterator const end, bool /*inverse*/)
{
    add_option(begin, end, "collapse");
}

void validator::
do_not_track_option(iterator const begin, iterator const end)
{
    error(begin, end, "deprecated option");
}

void validator::
begin_domain_option(iterator /*begin*/, iterator /*end*/)
{
    m_num_domains = 0;
}

void validator::
filter_domain(iterator const begin, iterator const end)
{
    validate_domain(begin, end);
    ++m_num_domains;
}

void validator::
end_domain_option(iterator const begin, iterator const end)
{
    if (m_num_domains == 0) {
        error(begin, end, "no domain is specified in domain option");
    }
    add_option(begin, end, "domain");
}

void validator::
site_key_option(iterator const begin, iterator const end,
                iterator const value_begin, iterator const value_end)
{
    if (value_begin == value_end) {
        error(begin, end, "empty sitekey");
    }
    add_option(begin, end, "sitekey");
}

void validator::
csp_option(iterator const begin, iterator const end,
           iterator /*value_begin*/, iterator /*value_end*/)
{
    add_option(begin, end, "csp");
}

void validator::
rewrite_option(iterator const begin, iterator const end,
               iterator const value_begin, iterator const value_end)
{
    if (value_begin == value_end) {
        error(begin, end, "empty rewrite option");
    }
    add_option(begin, end, "rewrite");
}

//
// element hide rule
//

// basic element hiding rule
void validator::
begin_basic_element_hiding_rule(iterator /*begin*/, iterator /*end*/) {}

void validator::
end_basic_element_hiding_rule(iterator /*begin*/, iterator /*end*/) {}

// exception element hiding rule
void validator::
begin_exception_element_hiding_rule(iterator /*begin*/, iterator /*end*/) {}

void validator::
end_exception_element_hiding_rule(iterator /*begin*/, iterator /*end*/) {}

// extended element hiding rule
void validator::
begin_extended_element_hiding_rule(iterator /*begin*/, iterator /*end*/) {}

void validator::
end_extended_element_hiding_rule(iterator /*begin*/, iterator /*end*/) {}

// element hiding domain(s)
void validator::
begin_filter_domains(iterator /*begin*/, iterator /*end*/) {}

void validator::
end_filter_domains(iterator /*begin*/, iterator /*end*/) {}

void validator::
css_selector(iterator begin, iterator end)
{
    if (begin == end) {
        error(begin, end, "empty CSS selector");
    }
}

//
// snippet filter rule
//
void validator::
begin_snippet_filter_rule(iterator /*begin*/, iterator /*end*/) {}

void validator::
end_snippet_filter_rule(iterator /*begin*/, iterator /*end*/) {}

void validator::
snippet(iterator begin, iterator end)
{
    if (begin == end) {
        error(begin, end, "empty snippet");
    }
}

//
// miscellaneous
//
void validator::
new_line(iterator bol, iterator eol)
{
    ++m_line_no;
    m_line = make_string_view(bol, eol);
}

void validator::
comment(iterator /*bol*/, iterator /*eol*/) {}

static void
print_error_line(std::ostream& os, std::string_view line, int64_t column, int64_t length)
{
    auto const c = static_cast<size_t>(column);
    auto const l = static_cast<size_t>(std::max(length - 1, 0l));

    os << '\t' << line << '\n'
       << '\t' << std::string(c, ' ')
       << '^' << std::string(l, '-') << '\n';
}

void validator::
error(iterator begin, iterator end, std::string_view msg)
{
    auto const column = begin - m_line.begin();
    auto const len = end - begin;

    std::cerr << '[' << m_line_no << ':' << column << ']'
              << ": " << msg << '\n';

    print_error_line(std::cerr, m_line, column, len);
}

void validator::
validate_address_pattern(iterator const begin, iterator const end)
// [[expects: begin <= end]]
{
    for (auto i1 = begin; i1 != end; ++i1) {
        if (std::iscntrl(*i1) || *i1 == '$') {
            error(i1, i1, "illegal character in pattern");
        }
        if (   starts_with(i1, end, "##")
            || starts_with(i1, end, "#@#")
            || starts_with(i1, end, "#?#")
            || starts_with(i1, end, "#$#"))
        {
            error(i1, i1 + 3, "element hiding delimiter in a filter pattern");
        }
    }
}

void validator::
validate_domain_pattern(iterator const begin, iterator const end)
{
    validate_address_pattern(begin, end);
}

void validator::
validate_regex_pattern(iterator const begin, iterator const end)
{
    try {
        std::regex { begin, end };
    }
    catch (std::regex_error const& e) {
        boost::format fmt { "regex error: %s" };
        error(begin, end, str(fmt % e.what()));
    }
}

void validator::
validate_domain(iterator const begin, iterator const end)
// [[expects: begin <= end]]
{
    if (begin == end) {
        error(begin, end, "empty domain");
        return;
    }

    auto i1 = begin;
    if (*i1 == '~') ++i1;

    validate_label(i1, end);
    while (i1 != end) {
        ++i1; // skip '.'
        validate_label(i1, end);
    }
}

void validator::
validate_label(iterator& begin, iterator const end)
{
    if (begin == end) {
        error(begin, end, "empty domain label");
        return;
    }

    if (!is_letter(*begin) && !std::isdigit(*begin)) {
        error(begin, begin + 1, "invalid character in a domain");
    }

    auto i1 = begin + 1;

    for (; i1 != end; ++i1) {
        if (*i1 == '.') break;

        if (!is_letter(*i1) && !std::isdigit(*i1) && *i1 != '-') {
            error(i1, i1 + 1, "invalid character in a domain");
        }
    }

    if (*(i1 - 1) == '-') {
        error(i1 - 1, i1, "invalid character in a domain");
    }

    begin = i1;
}

void validator::
add_option(iterator const begin, iterator const end,
           std::string_view const name)
{
    auto const [_, ok] = m_options.insert(name);
    if (!ok) {
        error(begin, end, "duplicate option");
    }
}

} // namespace adblock_parser

int
main(int argc, char* argv[])
{
    if (argc == 1) {
        std::cout << "Usage: " << argv[0] << " <filename>\n";
        return 1;
    }

    std::ostringstream oss;

    std::string_view filename { argv[1] };

    if (filename == "-") {
        oss << std::cin.rdbuf();
    }
    else {
        std::ifstream file;
        file.open(filename.data());
        if (file.fail()) {
            std::cerr << "can't open file: " << argv[1] << "\n";
            return 1;
        }

        oss << file.rdbuf();
    }

    adblock_parser::validator v;
    adblock_parser::parse_filter_list(oss.str(), v);

    return 0;
}
