#ifndef ADBLOCK_PARSER_HPP
#define ADBLOCK_PARSER_HPP

#include "adblock_parser/event_handler.hpp"

#include <string_view>

namespace adblock_parser {

// @return false on error
bool parse_filter(std::string_view, event_handler&);

// @return false on error
bool parse_filter_list(std::string_view, event_handler&);

} // namespace adblock_parser

#endif // ADBLOCK_PARSER_HPP
