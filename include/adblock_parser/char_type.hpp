#ifndef ADBLOCK_PARSER_CHAR_TYPE_HPP
#define ADBLOCK_PARSER_CHAR_TYPE_HPP

namespace adblock_parser {

using char_type = char const;
using iterator = char_type*;

} // namespace adblock_parser

#endif // ADBLOCK_PARSER_CHAR_TYPE_HPP
