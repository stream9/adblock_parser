#ifndef ADBLOCK_PARSER_JSON_HPP
#define ADBLOCK_PARSER_JSON_HPP

#include <stream9/json.hpp>

#include <string>
#include <string_view>

namespace adblock_parser {

namespace json { using namespace stream9::json; }

struct to_json_result
{
    json::array rules;
    json::array errors;

    operator bool() const { return errors.empty(); }
};

to_json_result
    to_json(std::string_view fliter_list) noexcept;

std::string
    from_json(json::array const& rules);

} // namespace adblock_parser

#endif // ADBLOCK_PARSER_JSON_HPP
