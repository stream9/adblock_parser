#ifndef ADBLOCK_PARSER_EVENT_HANDLER_HPP
#define ADBLOCK_PARSER_EVENT_HANDLER_HPP

#include "char_type.hpp"

#include <string_view>

namespace adblock_parser {

class event_handler
{
public:
    virtual ~event_handler() = default;

    //
    // filter rule
    //
    virtual void begin_basic_filter_rule(iterator /*bol*/, iterator /*eol*/) {}
    virtual void end_basic_filter_rule(iterator /*bol*/, iterator /*eol*/) {}

    virtual void begin_exception_filter_rule(iterator /*bol*/, iterator /*eol*/) {}
    virtual void end_exception_filter_rule(iterator /*bol*/, iterator /*eol*/) {}

    // address pattern
    virtual void basic_address_pattern(
        iterator /*begin*/, iterator /*end*/, bool /*prefix*/, bool /*suffix*/) {}

    virtual void domain_address_pattern(
        iterator /*begin*/, iterator /*end*/, bool /*suffix*/) {}

    virtual void regex_address_pattern(iterator /*begin*/, iterator /*end*/) {}

    // filter option
    virtual void begin_filter_options(iterator /*begin*/, iterator /*end*/) {}
    virtual void end_filter_options(iterator /*begin*/, iterator /*end*/) {}

    virtual void script_option(iterator /*begin*/, iterator /*end*/, bool /*inverse*/) {}
    virtual void image_option(iterator /*begin*/, iterator /*end*/, bool /*inverse*/) {}
    virtual void stylesheet_option(iterator /*begin*/, iterator /*end*/, bool /*inverse*/) {}
    virtual void object_option(iterator /*begin*/, iterator /*end*/, bool /*inverse*/) {}
    virtual void xmlhttprequest_option(iterator /*begin*/, iterator /*end*/, bool /*inverse*/) {}
    virtual void object_subrequest_option(iterator /*begin*/, iterator /*end*/, bool /*inverse*/) {}
    virtual void subdocument_option(iterator /*begin*/, iterator /*end*/, bool /*inverse*/) {}
    virtual void ping_option(iterator /*begin*/, iterator /*end*/, bool /*inverse*/) {}
    virtual void websocket_option(iterator /*begin*/, iterator /*end*/, bool /*inverse*/) {}
    virtual void webrtc_option(iterator /*begin*/, iterator /*end*/, bool /*inverse*/) {}
    virtual void popup_option(iterator /*begin*/, iterator /*end*/) {}
    virtual void media_option(iterator /*begin*/, iterator /*end*/, bool /*inverse*/) {}
    virtual void font_option(iterator /*begin*/, iterator /*end*/, bool /*inverse*/) {}
    virtual void other_option(iterator /*begin*/, iterator /*end*/, bool /*inverse*/) {}
    virtual void third_party_option(iterator /*begin*/, iterator /*end*/, bool /*inverse*/) {}
    virtual void match_case_option(iterator /*begin*/, iterator /*end*/) {}
    virtual void collapse_option(iterator /*begin*/, iterator /*end*/, bool /*inverse*/) {}
    virtual void do_not_track_option(iterator /*begin*/, iterator /*end*/) {}

    virtual void begin_domain_option(iterator /*begin*/, iterator /*end*/) {}
    virtual void filter_domain(iterator /*begin*/, iterator /*end*/) {}
    virtual void end_domain_option(iterator /*begin*/, iterator /*end*/) {}

    virtual void site_key_option(iterator /*begin*/, iterator /*end*/,
                           iterator /*value_begin*/, iterator /*value_end*/) {}
    virtual void csp_option(iterator /*begin*/, iterator /*end*/,
                           iterator /*value_begin*/, iterator /*value_end*/) {}
    virtual void rewrite_option(iterator /*begin*/, iterator /*end*/,
                           iterator /*value_begin*/, iterator /*value_end*/) {}

    virtual void document_option(iterator /*begin*/, iterator /*end*/, bool /*inverse*/) {}
    virtual void elem_hide_option(iterator /*begin*/, iterator /*end*/, bool /*inverse*/) {}
    virtual void generic_hide_option(iterator /*begin*/, iterator /*end*/) {}
    virtual void generic_block_option(iterator /*begin*/, iterator /*end*/) {}

    //
    // element hide rule
    //

    // basic element hiding rule
    virtual void begin_basic_element_hiding_rule(
                                    iterator /*begin*/, iterator /*end*/) {}
    virtual void end_basic_element_hiding_rule(
                                    iterator /*begin*/, iterator /*end*/) {}

    // exception element hiding rule
    virtual void begin_exception_element_hiding_rule(
                                    iterator /*begin*/, iterator /*end*/) {}
    virtual void end_exception_element_hiding_rule(
                                    iterator /*begin*/, iterator /*end*/) {}

    // extended element hiding rule
    virtual void begin_extended_element_hiding_rule(
                                    iterator /*begin*/, iterator /*end*/) {}
    virtual void end_extended_element_hiding_rule(
                                    iterator /*begin*/, iterator /*end*/) {}

    // element hiding domain(s)
    virtual void begin_filter_domains(iterator /*begin*/, iterator /*end*/) {}
    virtual void end_filter_domains(iterator /*begin*/, iterator /*end*/) {}

    virtual void css_selector(iterator /*begin*/, iterator /*end*/) {}

    //
    // snippet filter rule
    //
    virtual void begin_snippet_filter_rule(
                                    iterator /*begin*/, iterator /*end*/) {}
    virtual void end_snippet_filter_rule(
                                    iterator /*begin*/, iterator /*end*/) {}

    virtual void snippet(iterator /*begin*/, iterator /*end*/) {}

    //
    // miscellaneous
    //
    virtual void new_line(iterator, iterator) {}

    virtual void header(iterator/*bol*/, iterator/*eol*/,
                        iterator/*version_begin*/, iterator/*version_end*/) {}

    virtual void comment(iterator /*bol*/, iterator /*eol*/) {}

    virtual void error(iterator /*begin*/, iterator /*end*/, std::string_view /*msg*/) {}
};

} // namespace adblock_parser

#endif // ADBLOCK_PARSER_EVENT_HANDLER_HPP
