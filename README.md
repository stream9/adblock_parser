## Introduction

[Adblock Plus](https://www.adblockplus.org) compatible filter list parser.

It parse filter list and generate events so user can generate their own data
structure.

## Requirement

- C++17 compiler
- [cmake](https://cmake.org)
- [boost library](https://boost.org)
- [json library](https://gitlab.com/stream9/json)

## API

| path | description |
|------|-------------|
| [adblock_parser.hpp](include/adblock_parser.hpp) | parser entry point |
| [event_handler.hpp](include/adblock_parser/event_handler.hpp) | event handler interface |

## Example

- [validator.cpp](src/validator.cpp)